﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DotNetProject
{
    public static class Globals
    {
        // To store information about current employee
        public static User currentEmployee;

        // To validate if the current logged-in user has administration privileges or not
        public static bool IsAdmin = false;

        // To encrypt password inputs:
        public static string Encrypt(string password)
        {
            string hash = "D0tN3tPr0j3cT";
            byte[] data = UTF8Encoding.UTF8.GetBytes(password);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
                using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })    // TODO. Find alternative to CipherMode ECB (not recommended)
                {
                    ICryptoTransform transform = tripDes.CreateEncryptor();
                    byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
                    return Convert.ToBase64String(results, 0, results.Length);  //Encrypted password

                }
            }
        }

        // To decrypt the password from database:
        public static string Decrypt(string encPassword)
        {
            string hash = "D0tN3tPr0j3cT";
            byte[] data = Convert.FromBase64String(encPassword);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
                using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })    // TODO. Find alternative to CipherMode ECB (not recommended)
                {
                    ICryptoTransform transform = tripDes.CreateDecryptor();
                    byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
                    return UTF8Encoding.UTF8.GetString(results);  //Decrypted password
                }
            }
        }

        // To play the succes button sound effect:
        public static void PlaySuccessSFX()
        {
            try
            {
                SoundPlayer splayer = new SoundPlayer(Properties.Resources.Button_success2);
                splayer.Play();
            }
            catch (System.IO.FileNotFoundException ex)
            {
                Console.WriteLine("Error playing sound file: " + ex);
                return;
            }

        }

        // To play the error button sound effect:
        public static void PlayErrorSFX()
        {
            try
            {
                SoundPlayer splayer = new SoundPlayer(Properties.Resources.Button_error);
                splayer.Play();
            }
            catch (System.IO.FileNotFoundException ex)
            {
                Console.WriteLine("Error playing sound file: " + ex);
                return;
            }
        }      
    }
}
