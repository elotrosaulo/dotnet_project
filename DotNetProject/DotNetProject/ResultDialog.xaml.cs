﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class ResultDialog : Window
    {
        medlabsystemEntities ctx;
        public Case selCase;

        public ResultDialog()
        {
            InitializeComponent();
            ResetAllInputs();
        }

        private void BtSearch_Click(object sender, RoutedEventArgs e)
        {
            using (ctx = new medlabsystemEntities())
            try
            {
                string searchStr = tbSearch.Text;
                int idToSearch;

                if (searchStr == "") {
                    MessageBox.Show("Please insert a Case ID number", "Empty Case ID", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                if (!int.TryParse(searchStr, out idToSearch)) {
                    MessageBox.Show("Input error. Please insert a valid Case ID number (composed only by numbers from 0 to 9", "Invalid Case ID", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                var CaseByIdCol = (from c in ctx.Cases where c.Id == idToSearch select c).FirstOrDefault<Case>(); 
                selCase = CaseByIdCol;

                if (selCase == null)
                {
                    MessageBox.Show("File not found. Please insert an existing Case ID number", "Case Not Found", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                lblPatientID.Content = selCase.Patient_Id;

                var PatientFromCase = (from u in ctx.Users where u.Id.Equals(selCase.Patient_Id) select u).FirstOrDefault<User>();     
                if (PatientFromCase != null) {
                    User selPatient = PatientFromCase;
                    lblName.Content = selPatient.Name;
                    lblAge.Content = CalcAge(selPatient.DOB).ToString();
                    lblGender.Content = selPatient.Gender.ToString();
                    lblEmail.Content = selPatient.Email;
                    lblPhone.Content = selPatient.Phone;
                    string[] address = selPatient.Address.Split(';');
                    if (address.Length != 4) {
                        MessageBox.Show("Incorrect address format. Address should have 4 fields only", "Error reading address", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    lblAddressA.Content = address[0];
                    lblAddressB.Content = String.Format("{0} ({1}), {2}", address[1], address[2], address[3]);
                }
                
                var PhysicianFromCase = (from u in ctx.Users where u.Id.Equals(selCase.Physician_Id) select u).FirstOrDefault<User>(); 
                if (PhysicianFromCase != null) {
                    User selPhysician = PhysicianFromCase;
                    lblReferralPhysician.Content = "Dr. " + selPhysician.Name;
                    lblClinic.Content = selPhysician.Phys_Clinic;
                }
                lvPopulation(selCase);
            }
            catch (SystemException) // ex
            {
                MessageBox.Show("An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                // TODO: add ex.Message to logger file
            }
        }

        private void BtGoBack_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private int CalcAge(DateTime dob) {
            DateTime now = DateTime.Now;
            int Age;
            if (now.Month < dob.Month && now.Day < dob.Day) {
                Age = now.Year - dob.Year - 1;
            }
            else {
                Age = now.Year - dob.Year;
            }
            return Age;
        }

        private void ResetAllInputs() {
            lblPatientID.Content = "";
            lblName.Content = "";
            lblAge.Content = "";
            lblGender.Content = "";
            lblEmail.Content = "";
            lblPhone.Content = "";
            lblReferralPhysician.Content = "";
            lblClinic.Content = "";
            lblAddressA.Content = "";
            lblAddressB.Content = "";
            lvTests.ItemsSource = null;
            //lvTests.Items.Clear();
        }

        private void lvPopulation(Case selCase) {
            using (ctx = new medlabsystemEntities()) 
            {
                try
                {
                    var CasesResultCol = (from ct in ctx.CasesTests.Include("LabTest") where ct.Case_Id == selCase.Id select ct).ToList<CasesTest>();
                    lvTests.ItemsSource = CasesResultCol;
                }

                catch (SystemException) // ex
                {
                    MessageBox.Show("An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
        }

        private void LvTests_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lvTests.SelectedItem == null) {
                return;
            }

            CasesTest casesTest = (CasesTest) lvTests.SelectedItem;
            AddResultDialog addResultDialog = new AddResultDialog(selCase, casesTest);
            if (addResultDialog.ShowDialog() == true)
            {
                lvPopulation(selCase);
            }
        }

        private void BtClean_Click(object sender, RoutedEventArgs e)
        {
            tbSearch.Text = "";
            ResetAllInputs();
        }

        private void btDlgResultPrint_Click(object sender, RoutedEventArgs e)
        {
            if (selCase != null)
            {
                PrintResult printResult = new PrintResult(selCase);
                if (printResult.ShowDialog() == true)
                {
                    return;
                }
            }          
        }
    }
}
