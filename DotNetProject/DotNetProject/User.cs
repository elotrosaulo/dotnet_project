//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DotNetProject
{
    using System;
    using System.Collections.Generic;
    
    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            this.Cases = new HashSet<Case>();
            this.Cases1 = new HashSet<Case>();
            this.Cases2 = new HashSet<Case>();
        }
    
        public int Id { get; set; }
        public CategoryEnum Category { get; set; }
        public string Name { get; set; }
        public System.DateTime DOB { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public GenderEnum Gender { get; set; }
        public byte[] Photo { get; set; }
        public Nullable<System.DateTime> Empl_HireDate { get; set; }
        public Nullable<PositionEnum> Empl_Position { get; set; }
        public Nullable<decimal> Empl_Salary { get; set; }
        public Nullable<PhysSpecialtyEnum> Phys_Specialty { get; set; }
        public string Phys_Clinic { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Case> Cases { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Case> Cases1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Case> Cases2 { get; set; }
    }
}
