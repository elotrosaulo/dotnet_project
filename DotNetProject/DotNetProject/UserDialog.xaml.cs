﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for UserDIalog.xaml
    /// </summary>
    public partial class UserDialog : Window
    {
        public medlabsystemEntities ctx;
        private bool handle = true;     // To handle combobox automatic values
        private bool IsAdding = true;   // To handle Add / Update
        public string search;           // To send the content of the search textbox
        public enum RegionEnum { QC, NL, PE, NS, NB, ON, MB, SK, AB, BC, YT, NT, NU };
        Regex regexAddress = new Regex("^[A-Za-z0-9#. -]{1,78}$");
        Regex regexCity = new Regex("^[A-Za-z -]{1,20}$");
        Regex regexPostalCode = new Regex("^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$");
        Regex regexRegion = new Regex("^[A-Z]{2}$");
        DateTime realDOB, realDOH;

        public UserDialog(User user)
        {
            ctx = new medlabsystemEntities();
            InitializeComponent();
            comboCategory.ItemsSource = Enum.GetValues(typeof(CategoryEnum)).Cast<CategoryEnum>();
            comboGender.ItemsSource = Enum.GetValues(typeof(GenderEnum)).Cast<GenderEnum>();
            comboRegion.ItemsSource = Enum.GetValues(typeof(RegionEnum)).Cast<RegionEnum>();
            AllInputsDisabled();
            btDelete.IsEnabled = false;
        }

        private void BtSave_Click(object sender, RoutedEventArgs e)
        {
            string createdUser;
            FieldValidation();
            if(comboCategory.SelectedItem.Equals(CategoryEnum.Employee))
                FieldValidationEmp();

            using (ctx = new medlabsystemEntities()) 
            try
            {
                // General Fields / User
                int category = comboCategory.SelectedIndex + 1;     // Because enum starts from 1
                string name = tbName.Text;
                DateTime dob = (DateTime)dpBirthDate.SelectedDate;
                string email = tbEmail.Text;
                string phone = tbPhone.Text;
                string address = tbAddress.Text;
                string city = tbCity.Text;
                string postalCode = tbPostalCode.Text;
                string region = comboRegion.SelectedItem.ToString();

                int gender = comboGender.SelectedIndex + 1;
                byte[] photo = null;
                int empPostition = 0;
                decimal empSalary = 0;
                int physSpecialty = 0;
                string physClinic = "";
                string username = "";
                string password = "";
                DateTime? doh = null;                               // Nullable
                createdUser = "Patient";

                // Employee Fields
                if (category == 2)
                {
                    photo = bitmapImageToByteArray((BitmapImage)imgPhoto.Source);
                    empPostition = comboRight3.SelectedIndex + 1;
                    empSalary = decimal.Parse(tbSalary.Text);   // Validation code is down with all the field validations
                    username = tbUsername.Text;
                    password = Globals.Encrypt(tbPassword.Text);
                    doh = (DateTime)dpHireDate.SelectedDate;
                    createdUser = "Employee";
                }

                // Physician Fields
                if (category == 3)
                {
                    photo = bitmapImageToByteArray((BitmapImage)imgPhoto.Source);
                    physSpecialty = comboRight3.SelectedIndex + 1;
                    physClinic = tbClinic.Text;
                    username = tbUsername.Text;
                    password = tbPassword.Text;
                    createdUser = "Physician";
                }

                if (IsAdding)
                {
                    // ADD new user
                    User user = new User
                    {
                        Id = 0,
                        Category = (CategoryEnum)category,
                        Name = name,
                        DOB = dob,
                        Email = email,
                        Phone = phone,
                        Address = String.Join(";", address, city, region, postalCode),
                        Gender = (GenderEnum)gender,
                        Photo = photo,
                        Empl_HireDate = doh,
                        Empl_Position = (PositionEnum)empPostition,
                        Empl_Salary = empSalary,
                        Phys_Specialty = (PhysSpecialtyEnum)physSpecialty,
                        Phys_Clinic = physClinic,     
                        Username = username,
                        Password = password
                    };
                    ctx.Users.Add(user);                                // Insert operation is scheduled here but not excecuted yet.
                    ctx.SaveChanges();                                  // Sync objects in memory with database
                    Globals.PlaySuccessSFX();
                    MessageBox.Show(createdUser + " successfuly added to the database.", "Confirmation Message", MessageBoxButton.OK, MessageBoxImage.Information);
                    AllInputsReset();
                }
                else
                {
                    // UPDATE existing user
                    int id = (int)lblId.Content;
                    User user = (from u in ctx.Users where u.Id == id select u).FirstOrDefault<User>();

                    if (user != null)
                    {
                        user.Id = id;
                        user.Category = (CategoryEnum)category;
                        user.Name = name;
                        user.DOB = dob;
                        user.Email = email;
                        user.Phone = phone;
                        user.Address = String.Join(";", address, city, region, postalCode);
                        user.Gender = (GenderEnum)gender;
                        user.Photo = photo;
                        user.Empl_HireDate = doh;
                        user.Empl_Position = (PositionEnum)empPostition;
                        user.Empl_Salary = empSalary;
                        user.Phys_Specialty = (PhysSpecialtyEnum)physSpecialty;
                        user.Phys_Clinic = physClinic;     
                        user.Username = username;
                        user.Password = password;

                        ctx.SaveChanges();
                        Globals.PlaySuccessSFX();
                        MessageBox.Show(createdUser + " successfuly updated in the database.", "Confirmation Message", MessageBoxButton.OK, MessageBoxImage.Information);
                        AllInputsReset();
                    }
                    else
                    {
                        Console.WriteLine("Record to update not found");
                    }
                }
            }
            catch (DbEntityValidationException ex)          // UserValidation.cs
            {
                string errorMessage = "";
                foreach (var error in ex.EntityValidationErrors)
                {
                    foreach (var validationError in error.ValidationErrors)
                    {
                        errorMessage += validationError.ErrorMessage + "\n";
                    }
                }
                MessageBox.Show("Error adding / updating user: " + errorMessage, "Validation Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            catch (SystemException)
            {
                MessageBox.Show("An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                // TODO: add ex.Message to logger file
                return;
            }
            this.DialogResult = true;
        }

        private void BtCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        // DELETE User
        private void BtDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult result = MessageBox.Show("Do you want to delete the selected profile?", "Delete Profile", MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        {
                            if (IsAdding)
                            {
                                return;
                            }

                            int id = (int)lblId.Content;
                            User user = (from u in ctx.Users where u.Id == id select u).FirstOrDefault<User>();

                            if (user != null)
                            {
                                ctx.Users.Remove(user);
                                ctx.SaveChanges();
                                Globals.PlaySuccessSFX();
                                MessageBox.Show("File successfuly deleted from the database.", "Confirmation Message", MessageBoxButton.OK, MessageBoxImage.Information);
                                AllInputsReset();
                            }
                            else
                            {
                                Globals.PlayErrorSFX();
                                Console.WriteLine("Record to delete not found");
                            }
                            break;
                        }
                    case MessageBoxResult.No:
                        break;
                    default: // should not happen
                        MessageBox.Show("An unexpected error occurred. If this issue continues, please contact support.", "Internal error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                }
            }
            catch (SystemException) // ex
            {
                MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                // TODO: add ex.Message to logger file
            }

            this.DialogResult = true;
        }

        void AllInputsDisabled()
        {
            rectangleCover.Visibility = Visibility.Visible;
            btPhoto.Visibility = Visibility.Hidden;
            lblRight1.Visibility = Visibility.Hidden;
            tbUsername.Visibility = Visibility.Hidden;
            lblRight2.Visibility = Visibility.Hidden;
            tbPassword.Visibility = Visibility.Hidden;
            tbClinic.Visibility = Visibility.Hidden;
            lblRight3.Visibility = Visibility.Hidden;
            comboRight3.Visibility = Visibility.Hidden;
            lblRight4.Visibility = Visibility.Hidden;
            tbSalary.Visibility = Visibility.Hidden;
            lblHireDate.Visibility = Visibility.Hidden;
            dpHireDate.Visibility = Visibility.Hidden;

            //btSave.Content = "Add";
            btSaveImage.Source = new BitmapImage(new Uri(@"/images/plus.png", UriKind.Relative));
            btSave.IsEnabled = false;
        }

        void AllInputsEnabled()
        {
            rectangleCover.Visibility = Visibility.Hidden;
            btPhoto.Visibility = Visibility.Visible;
            lblRight1.Visibility = Visibility.Visible;
            tbUsername.Visibility = Visibility.Visible;
            lblRight2.Visibility = Visibility.Visible;
            tbPassword.Visibility = Visibility.Visible;
            tbClinic.Visibility = Visibility.Hidden;
            lblRight3.Visibility = Visibility.Visible;
            comboRight3.Visibility = Visibility.Visible;
            lblRight4.Visibility = Visibility.Visible;
            tbSalary.Visibility = Visibility.Visible;
            lblHireDate.Visibility = Visibility.Visible;
            dpHireDate.Visibility = Visibility.Visible;

            btSave.IsEnabled = true;
        }

        void AllInputsReset()
        {
            tbSearch.Text = "";
            lblId.Content = "-";
            tbName.Text = "";
            dpBirthDate.Text = "";
            comboGender.Text = "";
            tbEmail.Text = "";
            tbPhone.Text = "";
            tbAddress.Text = "";
            tbCity.Text = "";
            tbPostalCode.Text = "";
            comboRegion.Text = "";
            imgPhoto.Source = null;
            tbUsername.Text = "";
            tbPassword.Text = "";
            tbClinic.Text = "";
            comboRight3.Text = "";
            tbSalary.Text = "";
            dpHireDate.Text = "";
            lblErrorAddress.Visibility = Visibility.Hidden;
            lblErrorCity.Visibility = Visibility.Hidden;
            lblErrorClinic.Visibility = Visibility.Hidden;
            lblErrorDOB.Visibility = Visibility.Hidden;
            lblErrorDOH.Visibility = Visibility.Hidden;
            lblErrorEmail.Visibility = Visibility.Hidden;
            lblErrorGender.Visibility = Visibility.Hidden;
            lblErrorName.Visibility = Visibility.Hidden;
            lblErrorPassword.Visibility = Visibility.Hidden;
            lblErrorPCRegion.Visibility = Visibility.Hidden;
            lblErrorPhone.Visibility = Visibility.Hidden;
            lblErrorPosition.Visibility = Visibility.Hidden;
            lblErrorSalary.Visibility = Visibility.Hidden;
            lblErrorUsername.Visibility = Visibility.Hidden;

            btDelete.IsEnabled = false;
        }

        void PatientSelected()
        {
            AllInputsDisabled();
            rectangleCover.Visibility = Visibility.Hidden;
            btPhoto.Visibility = Visibility.Hidden;
            btSave.IsEnabled = true;
            btDelete.IsEnabled = false;
        }

        void EmployeeSelected()
        {
            AllInputsEnabled();
            rectangleCover.Visibility = Visibility.Hidden;
            lblRight1.Content = "Username :";
            lblRight2.Content = "Password :";
            tbPassword.Visibility = Visibility.Visible;
            tbClinic.Visibility = Visibility.Hidden;
            lblRight3.Content = "Position :";
            comboRight3.ItemsSource = Enum.GetValues(typeof(PositionEnum)).Cast<PositionEnum>();
        }

        void ReferralPhysicianSelected()
        {
            AllInputsEnabled();
            rectangleCover.Visibility = Visibility.Hidden;
            lblRight3.Content = "Specialty :";
            lblRight2.Content = "Clinic :";
            comboRight3.ItemsSource = Enum.GetValues(typeof(PhysSpecialtyEnum)).Cast<PhysSpecialtyEnum>();
            lblRight1.Visibility = Visibility.Hidden;
            tbUsername.Visibility = Visibility.Hidden;
            tbPassword.Visibility = Visibility.Hidden;
            tbClinic.Visibility = Visibility.Visible;
            lblRight4.Visibility = Visibility.Hidden;
            tbSalary.Visibility = Visibility.Hidden;
            lblHireDate.Visibility = Visibility.Hidden;
            dpHireDate.Visibility = Visibility.Hidden;
        }

        // Method to Handle Combobox Change Selection
        private void ComboCategory_DropDownClosed(object sender, EventArgs e)
        {
            if (handle) Handle();
            handle = true;
        }

        // Method to Handle Combobox Change Selection
        private void ComboCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combo = sender as ComboBox;
            handle = !combo.IsDropDownOpen;
            Handle();
        }

        // Method to Handle Combobox Change Selection
        private void Handle()
        {
            if (comboCategory.SelectedItem == null)
            {
                return;
            }

            switch (comboCategory.SelectedItem.ToString())
            {
                case "Patient":
                    {
                        PatientSelected();
                        AllInputsReset();
                        break;
                    }
                case "Employee":
                    {
                        if (Globals.IsAdmin == false) {
                            MessageBox.Show("Invalid option. Only manager can add a new employee profile", "Access Denied", MessageBoxButton.OK, MessageBoxImage.Hand);
                            comboCategory.SelectedItem = CategoryEnum.Patient;
                            PatientSelected();
                            AllInputsReset();
                            break;
                        }
                        EmployeeSelected();
                        AllInputsReset();
                        break;
                    }
                case "ReferralPhysician":
                    {
                        ReferralPhysicianSelected();
                        AllInputsReset();
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Internal Error. Invalid Category chosen");
                        break;
                    }
            }
        }

        // Method to Encode the Image
        private byte[] bitmapImageToByteArray(BitmapImage bitmapImage)
        {
            if (bitmapImage == null)
            {
                return null;
            }
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }
            return data;
        }

        private void BtPhoto_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files (*.gif; *.jpg; *.png)|*.gif; *.jpg; *.png";
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    imgPhoto.Source = new BitmapImage(new Uri(openFileDialog.FileName));
                }
                catch (InvalidOperationException ex)
                {
                    MessageBox.Show("Error importing the profile picture" + ex.Message, "Importing error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (UriFormatException ex)
                {
                    MessageBox.Show("Error importing the profile picture" + ex.Message, "Importing error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            search = tbSearch.Text;
            if (search == "")
            {
                //    return;
            }

            UserListDialog userListDialog = new UserListDialog(search);
            if (userListDialog.ShowDialog() == true)
            {

                User selectedUser = userListDialog.selectedUser;
                if (selectedUser == null)
                {
                    return;
                }

                if (Globals.IsAdmin == false && selectedUser.Category.Equals(CategoryEnum.Employee))
                {
                    MessageBox.Show("Invalid option. Only manager can edit an employee profile", "Access Denied", MessageBoxButton.OK, MessageBoxImage.Hand);
                    return;
                }

                comboCategory.SelectedItem = selectedUser.Category;
                Handle();

                lblId.Content = selectedUser.Id;
                tbName.Text = selectedUser.Name;
                dpBirthDate.SelectedDate = selectedUser.DOB;
                comboGender.SelectedItem = selectedUser.Gender;
                tbEmail.Text = selectedUser.Email;
                tbPhone.Text = selectedUser.Phone;

                string[] addressData = selectedUser.Address.Split(';');
                if (addressData.Length != 4)
                {
                    Console.WriteLine("Error reading from Address. Invalid number of fields, they should be 4.");
                }

                Match matchAddress = regexAddress.Match(addressData[0]);
                if (!matchAddress.Success)
                {
                    MessageBox.Show("Address should only contain letters, '#', '-' and spaces, and be 1 - 78 characters long. Please insert a valid address.", "Invalid Address", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                tbAddress.Text = addressData[0];

                Match matchCity = regexCity.Match(addressData[1]);
                if (!matchCity.Success)
                {
                    MessageBox.Show("City should only contain letters, '-', spaces, and be 1 - 20 characters long. Please insert a valid city.", "Invalid Address", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                tbCity.Text = addressData[1];

                Regex regexRegion = new Regex("[A-Z]{2}");
                Match matchRegion = regexRegion.Match(addressData[2]);
                if (!matchRegion.Success)
                {
                    MessageBox.Show("Region should only contain capital letters, and be 2 characters long. Please insert a valid region.", "Invalid Address", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                comboRegion.Text = addressData[2];

                Match matchPostalCode = regexPostalCode.Match(addressData[3]);
                if (!matchPostalCode.Success)
                {
                    MessageBox.Show("Postal Code should only contain capital letters, an space, and be 1 - 7 characters long. Please insert a valid postal code.", "Invalid Postal Code", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                tbPostalCode.Text = addressData[3];

                if (selectedUser.Photo != null)
                {
                    BitmapImage bmImg = new BitmapImage();
                    MemoryStream ms = new MemoryStream(selectedUser.Photo);
                    bmImg.BeginInit();
                    bmImg.StreamSource = ms;
                    bmImg.EndInit();
                    imgPhoto.Source = bmImg;
                }
                else
                {
                    imgPhoto.Source = null;
                }


                if (selectedUser.Category == CategoryEnum.Employee)
                {
                    tbUsername.Text = selectedUser.Username;
                    tbPassword.Text = Globals.Decrypt(selectedUser.Password);
                    comboRight3.SelectedItem = selectedUser.Empl_Position;
                    tbSalary.Text = selectedUser.Empl_Salary.ToString();
                    dpHireDate.SelectedDate = selectedUser.Empl_HireDate;
                }
                if (selectedUser.Category == CategoryEnum.ReferralPhysician)
                {
                    tbClinic.Text = selectedUser.Phys_Clinic;
                    comboRight3.SelectedItem = selectedUser.Phys_Specialty;
                }
                IsAdding = false;
                //btSave.Content = "Update";
                btSaveImage.Source = new BitmapImage(new Uri(@"/images/save.png", UriKind.Relative));
                btDelete.IsEnabled = true;
            }

        }

        private void TbName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!User.isNameValid(tbName.Text))
            {
                lblErrorName.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorName.Visibility = Visibility.Hidden;
            }
        }

        private void DpBirthDate_LostFocus(object sender, RoutedEventArgs e)
        {
            DateTime? birthDate = (dpBirthDate.SelectedDate).GetValueOrDefault();
            if (birthDate == null)
            {
                lblErrorDOB.Visibility = Visibility.Visible;
                return;
            }

            realDOB = birthDate.Value;

            if (!User.isDOBValid(realDOB))
            {
                lblErrorDOB.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorDOB.Visibility = Visibility.Hidden;
            }
        }

        private void ComboGender_LostFocus(object sender, RoutedEventArgs e)
        {
            if (comboGender.SelectedItem == null)
            {
                lblErrorGender.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorGender.Visibility = Visibility.Hidden;
            }
        }

        private void TbEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!User.isEmailValid(tbEmail.Text))
            {
                lblErrorEmail.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorEmail.Visibility = Visibility.Hidden;
            }
        }

        private void TbPhone_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!User.isPhoneValid(tbPhone.Text))
            {
                lblErrorPhone.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorPhone.Visibility = Visibility.Hidden;
            }
        }

        // Address field on the database is comoposed of these 4 fields (address, city, region and postal code)
        // That's the reason for making specific validation per field instead of database validation
        private void TbAddress_LostFocus(object sender, RoutedEventArgs e)
        {
            Match matchAddress = regexAddress.Match(tbAddress.Text);
            if (!matchAddress.Success)
            {
                lblErrorAddress.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorAddress.Visibility = Visibility.Hidden;
            }
        }

        private void TbCity_LostFocus(object sender, RoutedEventArgs e)
        {
            Match matchCity = regexCity.Match(tbCity.Text);
            if (!matchCity.Success)
            {
                lblErrorCity.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorCity.Visibility = Visibility.Hidden;
            }
        }

        private void TbPostalCode_LostFocus(object sender, RoutedEventArgs e)
        {
            Match matchPostalCode = regexPostalCode.Match(tbPostalCode.Text);
            if (!matchPostalCode.Success)
            {
                lblErrorPCRegion.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorPCRegion.Visibility = Visibility.Hidden;
            }
        }

        private void ComboRegion_LostFocus(object sender, RoutedEventArgs e)
        {
            Match matchRegion = regexRegion.Match(comboRegion.Text);
            if (!matchRegion.Success && (lblErrorPCRegion.Visibility == Visibility.Hidden)) // TODO. Check this if statement
            {
                lblErrorPCRegion.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorPCRegion.Visibility = Visibility.Hidden;
            }
        }

        private void TbUsername_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!User.isUsernameValid(tbUsername.Text))
            {
                lblErrorUsername.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorUsername.Visibility = Visibility.Hidden;
            }
        }

        private void TbPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!User.isPasswordValid(tbPassword.Text))
            {
                lblErrorPassword.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorPassword.Visibility = Visibility.Hidden;
            }
        }

        private void TbSalary_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!User.isEmpSalaryValid(tbSalary.Text))
            {
                lblErrorSalary.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorSalary.Visibility = Visibility.Hidden;
            }
        }

        private void DpHireDate_LostFocus(object sender, RoutedEventArgs e)
        {
            DateTime? hireDate = (dpHireDate.SelectedDate).GetValueOrDefault();
            if (hireDate == null)
            {
                lblErrorDOH.Visibility = Visibility.Visible;
                return;
            }

            realDOH = hireDate.Value;

            if (!User.isEmplHireDateValid(realDOH, realDOB))
            {
                lblErrorDOH.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorDOH.Visibility = Visibility.Hidden;
            }
        }

        void FieldValidation() {
            if (!User.isNameValid(tbName.Text))
            {
                lblErrorName.Visibility = Visibility.Visible;
            }
            DateTime? birthDate = (dpBirthDate.SelectedDate).GetValueOrDefault();
            if (birthDate == null)
            {
                lblErrorDOB.Visibility = Visibility.Visible;
            }
            realDOB = birthDate.Value;

            if (!User.isDOBValid(realDOB))
            {
                lblErrorDOB.Visibility = Visibility.Visible;
            }
            if (comboGender.SelectedItem == null)
            {
                lblErrorGender.Visibility = Visibility.Visible;
            }
            if (!User.isEmailValid(tbEmail.Text))
            {
                lblErrorEmail.Visibility = Visibility.Visible;
            }
            if (!User.isPhoneValid(tbPhone.Text))
            {
                lblErrorPhone.Visibility = Visibility.Visible;
            }
            Match matchAddress = regexAddress.Match(tbAddress.Text);
            if (!matchAddress.Success)
            {
                lblErrorAddress.Visibility = Visibility.Visible;
            }
            Match matchCity = regexCity.Match(tbCity.Text);
            if (!matchCity.Success)
            {
                lblErrorCity.Visibility = Visibility.Visible;
            }
            Match matchPostalCode = regexPostalCode.Match(tbPostalCode.Text);
            if (!matchPostalCode.Success)
            {
                lblErrorPCRegion.Visibility = Visibility.Visible;
            }
            Match matchRegion = regexRegion.Match(comboRegion.Text);
            if (!matchRegion.Success && (lblErrorPCRegion.Visibility == Visibility.Hidden)) // TODO. Check this if statement
            {
                lblErrorPCRegion.Visibility = Visibility.Visible;
            }
        }

        void FieldValidationEmp() {
            if (!User.isUsernameValid(tbUsername.Text))
            {
                lblErrorUsername.Visibility = Visibility.Visible;
            }
            if (!User.isPasswordValid(tbPassword.Text))
            {
                lblErrorPassword.Visibility = Visibility.Visible;
            }
            if (!User.isEmpSalaryValid(tbSalary.Text))
            {
                lblErrorSalary.Visibility = Visibility.Visible;
            }
            DateTime? hireDate = (dpHireDate.SelectedDate).GetValueOrDefault();
            if (hireDate == null)
            {
                lblErrorDOH.Visibility = Visibility.Visible;
            }
            realDOH = hireDate.Value;
            if (!User.isEmplHireDateValid(realDOH, realDOB))
            {
                lblErrorDOH.Visibility = Visibility.Visible;
            }
        }
    }
}
