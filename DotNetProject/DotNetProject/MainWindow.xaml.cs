﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public medlabsystemEntities ctx;

        public MainWindow()
        {
            ctx = new medlabsystemEntities();
            InitializeComponent();
            lblWelcome.Content = String.Format("Welcome {0}, how can I help you?", Globals.currentEmployee.Name);
        }

        private void BtCase_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            CaseDialog caseDialog = new CaseDialog();
            caseDialog.ShowDialog();
            this.Show();
        }

        private void BtUser_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            UserDialog userDialog = new UserDialog(null);
            userDialog.ShowDialog();
            this.Show();
        }

        private void BtTest_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            TestDialog testDialog = new TestDialog();
            testDialog.ShowDialog();
            this.Show();
        }

        private void BtResult_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            ResultDialog resultDialog = new ResultDialog();
            resultDialog.ShowDialog();
            this.Show();
        }

        private void BtAbout_Click(object sender, RoutedEventArgs e)
        {
            AboutDialog aboutDialog = new AboutDialog();
            aboutDialog.ShowDialog();
        }

        private void BtLogout_Click(object sender, RoutedEventArgs e)
        {
            LogInWindow logInWindow = new LogInWindow();
            this.Close();
            logInWindow.ShowDialog();
        }
    }
}
