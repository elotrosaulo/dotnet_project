﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetProject
{
    public partial class Case : IValidatableObject
    {
        static medlabsystemEntities ctx;
        public static bool isUserValid (int userId)
        {
            using (ctx = new medlabsystemEntities()){
                User userToValidate = (from user in ctx.Users where user.Id == userId select user).FirstOrDefault<User>();
                if (userToValidate == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }              
            }
        }
        public static bool isDueDateValid(DateTime dueDate)
        {
            return !(dueDate.CompareTo(DateTime.Now) < 0 || dueDate.CompareTo(DateTime.Now.AddDays(15)) > 0);
        }
        static public bool isTotalPaidValid(decimal? totalPaid)
        {
            return !(totalPaid < 0 || totalPaid > 100000);
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> result = new List<ValidationResult>();
            if (!isUserValid(Patient_Id)){
                result.Add(new ValidationResult("Patient not valid"));
            }
            if (!isUserValid(Physician_Id))
            {
                result.Add(new ValidationResult("ReferralPhysician not valid"));
            }
            if (!isUserValid(Employee_Id))
            {
                result.Add(new ValidationResult("Employee not valid"));
            }
            if (!isDueDateValid(Due_Date))
            {
                result.Add(new ValidationResult("Due date must be between 0-15 days from actual date"));
            }
            if (!isTotalPaidValid(Total_Paid))
            {
                result.Add(new ValidationResult("Total paid must be a valid number in the 0-100,000.00 range"));
            }
            return result;
        }
        /*
        public Nullable<decimal> Total_Cost;
        public Nullable<decimal> Tax_Amount { get { return Total_Cost * 0.15m;} }
        public Nullable<decimal> Net_Amount { get { return Total_Cost + Net_Amount; } }
        public Nullable<decimal> Balance { get { return Net_Amount - Total_Paid; } }
    */
    }
}
