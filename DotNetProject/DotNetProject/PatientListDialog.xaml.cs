﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for PatientList.xaml
    /// </summary>
    public partial class PatientListDialog : Window
    {
        medlabsystemEntities ctx;

        public PatientListDialog()
        {
            InitializeComponent();
            refreshPatientList();
        }

        void refreshPatientList()
        {
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    var PatientCol = (from pat in ctx.Users where pat.Category == CategoryEnum.Patient select pat).ToList<User>();
                    lvPatListDlgPatientList.ItemsSource = PatientCol;
                    lvPatListDlgPatientList.Items.Refresh();
                    lvPatListDlgPatientList.SelectedItem = null;
                }
                catch (SystemException) //ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
        }

        private void btPatListSearchByName_Click(object sender, RoutedEventArgs e)
        {
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    string searchStr = tbPatListDlgSearch.Text;
                    var PatientByNameCol = (from pat in ctx.Users where ((pat.Category == CategoryEnum.Patient) && (pat.Name.Contains(searchStr))) select pat).ToList<User>();
                    lvPatListDlgPatientList.ItemsSource = PatientByNameCol;
                    lvPatListDlgPatientList.Items.Refresh();
                    lvPatListDlgPatientList.SelectedItem = null;
                }
                catch (SystemException) //ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
        }

        private void btPatListSearchByPhone_Click(object sender, RoutedEventArgs e)
        {
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    string searchStr = tbPatListDlgSearch.Text;
                    var PatientByPhoneCol = (from pat in ctx.Users where ((pat.Category == CategoryEnum.Patient) && (pat.Phone.Contains(searchStr))) select pat).ToList<User>();
                    lvPatListDlgPatientList.ItemsSource = PatientByPhoneCol;
                    lvPatListDlgPatientList.Items.Refresh();
                    lvPatListDlgPatientList.SelectedItem = null;
                }
                catch (SystemException) //ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
        }

        private void lvPatListDlgPatientList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            User currPatient = (User)lvPatListDlgPatientList.SelectedItem;
            CaseDialog.currPatCase = currPatient;
            this.DialogResult = true; // close actual window       
        }

        private void btPatListDlgErase_Click(object sender, RoutedEventArgs e)
        {
            tbPatListDlgSearch.Text = "";
            refreshPatientList();
        }

        private void btPatListDlgClose_Click(object sender, RoutedEventArgs e)
        {
            CaseDialog.currPatCase = null;
            this.DialogResult = true; // close actual window
        }
    }
}
