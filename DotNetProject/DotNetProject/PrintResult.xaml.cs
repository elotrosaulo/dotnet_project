﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for PrintResult.xaml
    /// </summary>
    public partial class PrintResult : Window
    {
        medlabsystemEntities ctx;
        Case caseToPrint;
        List<CasesTest> ResultListToPrint = new List<CasesTest>();
        public PrintResult(Case caseToPrint)
        {
            InitializeComponent();
            this.caseToPrint = caseToPrint;
            // load list of Lab Tests
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    var CasesResultCol = (from ct in ctx.CasesTests.Include("LabTest") where ct.Case_Id == caseToPrint.Id select ct).ToList<CasesTest>();
                    lvPrintResultLabTestList.ItemsSource = CasesResultCol;
                }
                catch (SystemException) //ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
            // load case information
            lblPrintResultCaseId.Content = caseToPrint.Id;
            lblPrintResultDate.Content = caseToPrint.Transaction_Date;
            lblPrintResultPatName.Content = caseToPrint.User.Name; // User = Patient
            lblPrintResultPatId.Content = caseToPrint.User.Id;
            string[] address = caseToPrint.User.Address.Split(';');
            if (address.Length == 4)
            {
                lblPrintResultPatAddress.Content = String.Format("{0}, {1} ({2}), {3}", address[0], address[1], address[2], address[3]);
            }      
            lblPrintResultPatPhone.Content = caseToPrint.User.Phone;
            lblPrintResultPatEmail.Content = caseToPrint.User.Email;
            lblPrintResultPhys.Content = String.Format("Dr. {0}", caseToPrint.User1.Name);  // User1 = Physician
        }

        private void btPrintResultPrint_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.IsEnabled = false;
                PrintDialog printDialog = new PrintDialog();
                if (printDialog.ShowDialog() == true)
                {
                    printDialog.PrintVisual(printResult, "PrintResult");
                }
            }
            catch (Exception) // ex
            {
                MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Printing error", MessageBoxButton.OK, MessageBoxImage.Error);
                // TODO: add ex.Message to logger file
            }
            finally
            {
                this.IsEnabled = true;
            }
        }
    }
}
