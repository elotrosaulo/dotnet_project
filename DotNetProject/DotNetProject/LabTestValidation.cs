﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DotNetProject
{
    partial class LabTest : IValidatableObject
    {
        static public bool isTestNameValid(string name)
        {
            return !(string.IsNullOrWhiteSpace(name) || string.IsNullOrEmpty(name) || !Regex.Match(name, @"^[A-Za-z0-9 -]{1,100}").Success);
        }
        static public bool isUnitValid(string unit)
        {
            return !(string.IsNullOrWhiteSpace(unit) || string.IsNullOrEmpty(unit) || !Regex.Match(unit, @"^[A-Za-z\/%\u00b5]{1,20}").Success);
        }
        static public bool isMinimumValid(double minimum)
        {
            return !((minimum < 0) || (minimum >= 100000) || double.IsNaN(minimum));
        }
        static public bool isMaximumValid(double maximum)
        {
            return !((maximum <= 0) || (maximum > 100000) || double.IsNaN(maximum));
        }
        static public bool isPriceValid(decimal price)
        {
            return !((price <= 0) || (price > 100000));
        }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> result = new List<ValidationResult>();         
            if (!isTestNameValid(Name))
            {
                result.Add(new ValidationResult("Test name must be 1-100 characters, composed with letters, digits, '-' and space"));
            }
            if (!isUnitValid(Unit))
            {
                result.Add(new ValidationResult("Unit must be 1-20 characters, composed with letters, µ, % and /"));
            }
            if (!(isMinimumValid(Minimum)))
            {
                result.Add(new ValidationResult("Minimum must be a valid number in the 0-100,000 range"));
            }
            if(!(isMaximumValid(Maximum)))
            {
                result.Add(new ValidationResult("Maximum must be a valid number in the 0-100,000 range"));
            }
            if (Maximum <= Minimum)
            {
                result.Add(new ValidationResult("Maximum must be greater than minimum"));
            }
            if(!(isPriceValid(Price)))
            {
                result.Add(new ValidationResult("Price must be a valid number in the 0-100,000.00 range"));
            }
            return result;
        }

        public string ReferenceRange { get { return Minimum + " - " + Maximum + " " + Unit; }}
    }
}
