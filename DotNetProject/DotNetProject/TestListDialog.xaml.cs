﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for TestListDialog.xaml
    /// </summary>
    public partial class TestListDialog : Window
    {
        medlabsystemEntities ctx;
        public TestListDialog()
        {
            InitializeComponent();
            refreshLabTestList();
        }

        void refreshLabTestList()
        {
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    var LabTestCol = (from lt in ctx.LabTests select lt).ToList<LabTest>();
                    lvTestListDlgLabTests.ItemsSource = LabTestCol;
                    lvTestListDlgLabTests.Items.Refresh();
                    lvTestListDlgLabTests.SelectedItem = null;
                }
                catch (SystemException) // ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }                
        }

        private void lvLabTests_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            LabTest currTest = (LabTest)lvTestListDlgLabTests.SelectedItem;
            TestDialog.currTest = currTest; // assign selected test to currTest
            this.DialogResult = true; // close actual window
        }

        private void btTestListDlgSearchByName_Click(object sender, RoutedEventArgs e)
        {
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    string searchStr = tbTestDlgSearch.Text;
                    var LabTestByNameCol = (from lt in ctx.LabTests where lt.Name.Contains(searchStr) select lt).ToList<LabTest>();
                    lvTestListDlgLabTests.ItemsSource = LabTestByNameCol;
                    lvTestListDlgLabTests.Items.Refresh();
                    lvTestListDlgLabTests.SelectedItem = null;
                }
                catch (SystemException) // ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }             
        }

        private void btTestListDlgSearchByCat_Click(object sender, RoutedEventArgs e)
        {
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    string searchStr = tbTestDlgSearch.Text;
                    var LabTestByNameCol = (from lt in ctx.LabTests where lt.Category.ToString().Contains(searchStr) select lt).ToList<LabTest>();
                    lvTestListDlgLabTests.ItemsSource = LabTestByNameCol;
                    lvTestListDlgLabTests.Items.Refresh();
                    lvTestListDlgLabTests.SelectedItem = null;
                }
                catch (SystemException) // ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }               
        }

        private void btTeslListDlgErase_Click(object sender, RoutedEventArgs e)
        {
            tbTestDlgSearch.Text = "";
            refreshLabTestList();
        }

        private void btTestListDlgClose_Click(object sender, RoutedEventArgs e)
        {
            TestDialog.currTest = null;
            this.DialogResult = true; // close actual window            
        }
    }
}
