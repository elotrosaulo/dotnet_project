﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for TestDialog.xaml
    /// </summary>
    public partial class TestDialog : Window
    {
        public static LabTest currTest;
        medlabsystemEntities ctx;
        public TestDialog() 
        { 
            InitializeComponent();
            // Feed combo lists with LabTestCategoryEnum and LabTestSampleEnum
            comboTestDlgCategory.ItemsSource = Enum.GetValues(typeof(LabTestCategoryEnum));
            comboTestDlgCategory.SelectedItem = comboTestDlgCategory.Items[0];
            comboTestDlgSample.ItemsSource = Enum.GetValues(typeof(LabTestSampleEnum));
            comboTestDlgSample.SelectedItem = comboTestDlgSample.Items[0];
            resetAllFields();
        }

        private void btSearch_Click(object sender, RoutedEventArgs e)
        {
            TestListDialog testListDialog = new TestListDialog();
            if (testListDialog.ShowDialog() == true)
            {
                if (currTest != null)
                { // Assign selected test to currTest and load information to the form
                    lblTestDlgTestId.Content = currTest.Id;
                    tbTestDlgName.Text = currTest.Name;
                    comboTestDlgCategory.SelectedItem = currTest.Category;
                    tbTestDlgUnit.Text = currTest.Unit;
                    tbTestDlgMin.Text = currTest.Minimum.ToString();
                    tbTestDlgMax.Text = currTest.Maximum.ToString();
                    comboTestDlgSample.SelectedItem = currTest.Sample;
                    tbTestDlgPrice.Text = currTest.Price.ToString();
                    btTestDlgDelete.IsEnabled = true;
                    imageTestDlgSaveBt.Source = new BitmapImage(new Uri(@"/images/save.png", UriKind.Relative));    
                }
            }
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            // Recover all information from the form fields
            string name = tbTestDlgName.Text;
            if (!LabTest.isTestNameValid(name))
            {
                lblTestDlgErrorName.Visibility = Visibility.Visible;
                return;
            }
            LabTestCategoryEnum category = (LabTestCategoryEnum)comboTestDlgCategory.SelectedItem;
            string unit = tbTestDlgUnit.Text;
            if (!LabTest.isUnitValid(unit))
            {
                lblTestDlgErrorUnit.Visibility = Visibility.Visible;
                return;
            }
            double min, max;
            if (!double.TryParse(tbTestDlgMin.Text, out min) || !LabTest.isMinimumValid(min))
            {
                lblTestDlgErrorMin.Visibility = Visibility.Visible;
                return;
            }
            if (!double.TryParse(tbTestDlgMax.Text, out max) || !LabTest.isMaximumValid(max))
            {
                lblTestDlgErrorMax.Visibility = Visibility.Visible;
                return;
            }
            LabTestSampleEnum sample = (LabTestSampleEnum)comboTestDlgSample.SelectedItem;
            decimal price;
            if (!decimal.TryParse(tbTestDlgPrice.Text, out price) || !LabTest.isPriceValid(price))
            {
                lblTestDlgErrorPrice.Visibility = Visibility.Visible;
                return;
            }
            
            if (currTest != null)
            {
                // Update LabTest
                using (ctx = new medlabsystemEntities())
                {
                    int currId = currTest.Id;
                    try
                    {
                        LabTest testToUpdate = (from test in ctx.LabTests where test.Id == currId select test).FirstOrDefault<LabTest>();
                        if (testToUpdate != null)
                        {
                            testToUpdate.Name = name;
                            testToUpdate.Category = category;
                            testToUpdate.Unit = unit;
                            testToUpdate.Minimum = min;
                            testToUpdate.Maximum = max;
                            testToUpdate.Sample = sample;
                            testToUpdate.Price = price;
                            ctx.SaveChanges();
                            Globals.PlaySuccessSFX();
                            MessageBox.Show(this, "Test updated with success.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                            resetAllFields(); // clear form
                        }
                        else
                        {
                            MessageBox.Show("Record to update not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information); // Fix me??
                        }
                    }
                    catch (DbEntityValidationException ex) // This should not happen! Comment return statements to be able to test it.
                    {
                        string errorMessage = "";
                        foreach (var error in ex.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                            {
                                errorMessage += validationError.ErrorMessage + "\n";
                            }
                        }
                        MessageBox.Show(this, errorMessage, "Validation Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return; // keep dialog open
                    }
                    catch (SystemException) // ex
                    {
                        MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                        // TODO: add ex.Message to logger file
                    }
                }
            }
            else
            {
                // Add new LabTest
                using (ctx = new medlabsystemEntities())
                {
                    try
                    {
                        LabTest test = new LabTest { Name = name, Category = category, Unit = unit, Minimum = min, Maximum = max, Sample = sample, Price = price };
                        ctx.LabTests.Add(test);
                        ctx.SaveChanges();
                        Globals.PlaySuccessSFX();
                        MessageBox.Show(this, "Test created with success.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                        resetAllFields();
                    }
                    catch (DbEntityValidationException ex)  // This should never happen! Comment return statments to be able to test it.
                    {
                        string errorMessage = "";
                        foreach (var error in ex.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                            {
                                errorMessage += validationError.ErrorMessage + "\n";
                            }
                        }
                        MessageBox.Show(this, errorMessage, "Validation Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    catch (SystemException) // ex
                    {
                        MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                        // TODO: add ex.Message to logger file
                    }
                }
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            if (currTest != null)
            {
                MessageBoxResult result = MessageBox.Show(this, "Do you want to delete selected test?", "Exit Program", MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        using (ctx = new medlabsystemEntities())
                        {
                            try
                            {
                                int currId = currTest.Id;
                                LabTest testToDelete = (from lt in ctx.LabTests where lt.Id == currId select lt).FirstOrDefault<LabTest>();
                                if (testToDelete != null)
                                {
                                    ctx.LabTests.Remove(testToDelete);
                                    ctx.SaveChanges();
                                    Globals.PlaySuccessSFX();
                                    MessageBox.Show(this, "Test deleted.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                    resetAllFields();
                                }
                                else
                                {
                                    MessageBox.Show("Record to delete not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                }
                            }
                            catch (SystemException) // ex
                            {
                                MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                                // TODO: add ex.Message to logger file
                            }
                        }
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        Console.WriteLine("This shouldn't happen!");
                        return;
                }
            }
        }

        void resetAllFields()
        {
            currTest = null;
            lblTestDlgTestId.Content = "";
            tbTestDlgName.Text = "";
            comboTestDlgCategory.SelectedItem = comboTestDlgCategory.Items[0];
            tbTestDlgUnit.Text = "";
            tbTestDlgMin.Text = "";
            tbTestDlgMax.Text = "";
            comboTestDlgSample.SelectedItem = comboTestDlgSample.Items[0];
            tbTestDlgPrice.Text = "";
            btTestDlgDelete.IsEnabled = false;
            imageTestDlgSaveBt.Source = new BitmapImage(new Uri(@"/images/plus.png", UriKind.Relative));
            lblTestDlgErrorName.Visibility = Visibility.Hidden;
            lblTestDlgErrorUnit.Visibility = Visibility.Hidden;
            lblTestDlgErrorMin.Visibility = Visibility.Hidden;
            lblTestDlgErrorMax.Visibility = Visibility.Hidden;
            lblTestDlgErrorPrice.Visibility = Visibility.Hidden;
        }

        private void btTestDlgErase_Click(object sender, RoutedEventArgs e)
        {
            resetAllFields();
        }

        private void tbTestDlgName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!LabTest.isTestNameValid(tbTestDlgName.Text))
            {
                lblTestDlgErrorName.Visibility = Visibility.Visible;
            }
            else
            {
                lblTestDlgErrorName.Visibility = Visibility.Hidden;
            }
        }

        private void tbTestDlgUnit_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!LabTest.isUnitValid(tbTestDlgUnit.Text))
            {
                lblTestDlgErrorUnit.Visibility = Visibility.Visible;
            }
            else
            {
                lblTestDlgErrorUnit.Visibility = Visibility.Hidden;
            }
        }

        private void tbTestDlgMin_LostFocus(object sender, RoutedEventArgs e)
        {
            double min;
            if (!double.TryParse(tbTestDlgMin.Text, out min) || !LabTest.isMinimumValid(min))
            {
                lblTestDlgErrorMin.Visibility = Visibility.Visible;
            }
            else
            {
                lblTestDlgErrorMin.Visibility = Visibility.Hidden;
            }
        }

        private void tbTestDlgMax_LostFocus(object sender, RoutedEventArgs e)
        {
            double max;
            if (!double.TryParse(tbTestDlgMax.Text, out max) || !LabTest.isMaximumValid(max))
            {
                lblTestDlgErrorMax.Visibility = Visibility.Visible;
            }
            else
            {
                lblTestDlgErrorMax.Visibility = Visibility.Hidden;
            }
        }

        private void tbTestDlgPrice_LostFocus(object sender, RoutedEventArgs e)
        {
            decimal price;
            if (!decimal.TryParse(tbTestDlgPrice.Text, out price) || !LabTest.isPriceValid(price))
            {
                lblTestDlgErrorPrice.Visibility = Visibility.Visible;
            }
            else
            {
                lblTestDlgErrorPrice.Visibility = Visibility.Hidden;
            }
        }
    }
}