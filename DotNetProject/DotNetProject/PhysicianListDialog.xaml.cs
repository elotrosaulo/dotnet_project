﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for PhysicianListDialog.xaml
    /// </summary>
    public partial class PhysicianListDialog : Window
    {
        medlabsystemEntities ctx;
        public PhysicianListDialog()
        {
            InitializeComponent();
            refreshPhysicianList();
        }

        void refreshPhysicianList()
        {
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    var PhysicianCol = (from phys in ctx.Users where phys.Category == CategoryEnum.ReferralPhysician select phys).ToList<User>();
                    lvPhysListDlgPhysicianList.ItemsSource = PhysicianCol;
                    lvPhysListDlgPhysicianList.Items.Refresh();
                    lvPhysListDlgPhysicianList.SelectedItem = null;
                }
                catch (SystemException) //ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
        }

        private void btPhysListSearchByName_Click(object sender, RoutedEventArgs e)
        {
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    string searchStr = tbPhysListDlgSearch.Text;
                    var PhysByNameCol = (from phys in ctx.Users where ((phys.Category == CategoryEnum.ReferralPhysician) && (phys.Name.Contains(searchStr))) select phys).ToList<User>();
                    lvPhysListDlgPhysicianList.ItemsSource = PhysByNameCol;
                    lvPhysListDlgPhysicianList.Items.Refresh();
                    lvPhysListDlgPhysicianList.SelectedItem = null;
                }
                catch (SystemException) //ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
        }

        private void btPhysListSearchBySpecialty_Click(object sender, RoutedEventArgs e)
        {
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    string searchStr = tbPhysListDlgSearch.Text;
                    var PhysBySpecialtyCol = (from phys in ctx.Users where ((phys.Category == CategoryEnum.ReferralPhysician) && (phys.Phys_Specialty.ToString().Contains(searchStr))) select phys).ToList<User>();
                    lvPhysListDlgPhysicianList.ItemsSource = PhysBySpecialtyCol;
                    lvPhysListDlgPhysicianList.Items.Refresh();
                    lvPhysListDlgPhysicianList.SelectedItem = null;
                }
                catch (SystemException) //ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
        }

        private void lvPhysListDlgPhysicianList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            User currPhysician = (User)lvPhysListDlgPhysicianList.SelectedItem;
            CaseDialog.currPhysCase = currPhysician;
            this.DialogResult = true; // close actual window  
        }

        private void btPhysListDlgErase_Click(object sender, RoutedEventArgs e)
        {
            tbPhysListDlgSearch.Text = "";
            refreshPhysicianList();
        }

        private void btPhysListDlgClose_Click(object sender, RoutedEventArgs e)
        {
            CaseDialog.currPhysCase = null;
            this.DialogResult = true; // close actual window
        }
    }
}
