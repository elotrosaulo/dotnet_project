﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for UserListDialog.xaml
    /// </summary>
    public partial class UserListDialog : Window
    {
        medlabsystemEntities ctx;
        public User selectedUser;
        public string searchStr;

        public UserListDialog(string search)
        {
            ctx = new medlabsystemEntities();
            this.searchStr = search;
            InitializeComponent();
            tbSearch.Text = searchStr;
        }

        private void btSearchByName_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                searchStr = tbSearch.Text;
                var UserByNameCol = (from u in ctx.Users where u.Name.Contains(searchStr) select u).ToList<User>(); 
                lvUsers.ItemsSource = UserByNameCol;
                lvUsers.Items.Refresh();
                lvUsers.SelectedItem = null;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btSearchByCategory_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string searchStr = tbSearch.Text;
                var UserByNameCol = (from u in ctx.Users where u.Category.ToString().Contains(searchStr) select u).ToList<User>(); //FIX ME!!
                lvUsers.ItemsSource = UserByNameCol;
                lvUsers.Items.Refresh();
                lvUsers.SelectedItem = null;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btErase_Click(object sender, RoutedEventArgs e)
        {
            tbSearch.Text = "";
            refreshLabTestList();
        }

        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        void refreshLabTestList()
        {
            try
            {
                var UserByNameCol = (from u in ctx.Users select u).ToList<User>();
                lvUsers.ItemsSource = UserByNameCol;
                lvUsers.Items.Refresh();
                lvUsers.SelectedItem = null;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void lvUsers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            selectedUser = (User)lvUsers.SelectedItem;
            this.DialogResult = true; // close actual window
        }
    }
}
