﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for AddResultDialog.xaml
    /// </summary>
    public partial class AddResultDialog : Window
    {
        medlabsystemEntities ctx;
        public CasesTest casesTest;
        private Case selectedCase;
        public LabTest selectedTest;

        public AddResultDialog(Case selCase, CasesTest selCasesTest)
        {
            ctx = new medlabsystemEntities();
            this.selectedCase = selCase;
            this.casesTest = selCasesTest;
            selectedTest = (from lt in ctx.LabTests where lt.Id == selCasesTest.Test_Id select lt).FirstOrDefault<LabTest>();
            InitializeComponent();
            ResetAllFields();
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double result;
                if (!double.TryParse(tbTestResult.Text, out result) || tbTestResult.Text == "")
                {
                    lblErrorResult.Visibility = Visibility.Visible;
                    MessageBox.Show("Please insert a valid Test result", "Invalid Result", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                    
                int currCaseId = casesTest.Case_Id;
                int currTestId = casesTest.Test_Id;
                CasesTest casesTestToUpdate = (from ct in ctx.CasesTests where ct.Case_Id == currCaseId && ct.Test_Id == currTestId select ct).FirstOrDefault<CasesTest>();
                if (casesTestToUpdate != null)
                {
                    casesTestToUpdate.Status = CasesTestStatusEnum.Done;
                    casesTestToUpdate.Result = result;
                    ctx.SaveChanges();
                    Console.WriteLine("Result updated in database");
                    this.DialogResult = true;
                }
                else
                {
                    MessageBox.Show("Record to update not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (SystemException) // ex
            {
                MessageBox.Show("An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                // TODO: add ex.Message to logger file
            }
        }

        private void BtDelete_Click(object sender, RoutedEventArgs e)
        {
            ResetAllFields();
        }

        private void BtCancel_Click(object sender, RoutedEventArgs e)
        {
            ResetAllFields();
            Close();
        }

        private void ResetAllFields(){
            lblTestId.Content = casesTest.Test_Id.ToString();
            lblTestName.Content = selectedTest.Name;
            lblCategory.Content = selectedTest.Category;
            lblUnit.Content = selectedTest.Unit;
            lblMinimum.Content = selectedTest.Minimum;
            lblMaximum.Content = selectedTest.Maximum;
            lblSample.Content = selectedTest.Sample.ToString();
            tbTestResult.Text = casesTest.Result.ToString();
            lblErrorResult.Visibility = Visibility.Hidden;
        }

        private void TbTestResult_LostFocus(object sender, RoutedEventArgs e)
        {
            double result;
            if (!double.TryParse(tbTestResult.Text, out result))
            {
                lblErrorResult.Visibility = Visibility.Visible;
            }
            if (!CasesTest.isResultValid(result))
            {
                lblErrorResult.Visibility = Visibility.Visible;
            }
            else
            {
                lblErrorResult.Visibility = Visibility.Hidden;
            }
        }
    }
}
