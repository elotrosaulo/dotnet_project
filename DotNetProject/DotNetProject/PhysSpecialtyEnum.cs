//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DotNetProject
{
    using System;
    
    public enum PhysSpecialtyEnum : int
    {
        Anesthesiology = 1,
        Allergy = 2,
        Cardiology = 3,
        Dermatology = 4,
        EmergencyMedicine = 5,
        Endocrinology = 6,
        Gastroenterology = 7,
        Genetic = 8,
        Gynecology = 9,
        Geriatrics = 10,
        Hematology = 11,
        Infectology = 12,
        Nephrology = 13,
        Neurology = 14,
        Obstetrics = 15,
        Oncology = 16,
        Otolaryngology = 17,
        Pediatrics = 18,
        Psychiatry = 19,
        Respirology = 20,
        Rheumatology = 21,
        Urology = 22
    }
}
