﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DotNetProject
{
    partial class User : IValidatableObject
    {
        static public bool isNameValid(string name)
        {
            return !(string.IsNullOrWhiteSpace(name) || string.IsNullOrEmpty(name) || !Regex.Match(name, @"^[A-Za-z0-9 -][^#&<>\~@;$^%{ }?]{1,50}?").Success);
        }
        static public bool isDOBValid(DateTime dob)
        {
            return !(dob.Year < 1900 || dob.Year > 2100);
        }
        static public bool isEmailValid(string email)
        {
            string regexEmailStr = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*" + "@" + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$";
            return !(string.IsNullOrWhiteSpace(email) || string.IsNullOrEmpty(email) || !Regex.Match(email, regexEmailStr).Success);
        }
        static public bool isPhoneValid(string phone)
        {
            return !(string.IsNullOrWhiteSpace(phone) || string.IsNullOrEmpty(phone) || !Regex.Match(phone, @"^[0-9 -]{1,15}?").Success);
        }
        static public bool isAddressValid(string address)
        {
            return !(string.IsNullOrWhiteSpace(address) || string.IsNullOrEmpty(address) || !Regex.Match(address, @"^[A-Za-z0-9 -][^<>\~@;$^%{ }?]{1,100}?").Success);
        }
        static public bool isEmplHireDateValid(DateTime hireDate, DateTime dob)
        {
            return !(hireDate.Year < 1950 || hireDate.Year > 2100 || hireDate.Year <= (dob.Year + 17));
        }
        static public bool isEmpSalaryValid(string salary)
        {
            return !(string.IsNullOrWhiteSpace(salary) || string.IsNullOrEmpty(salary) || !Regex.Match(salary, @"^[0-9]{1,10}?").Success);
        }
        static public bool isPhysClinicValid(string clinic)
        {
            return !(string.IsNullOrWhiteSpace(clinic) || string.IsNullOrEmpty(clinic) || !Regex.Match(clinic, @"^[A-Za-z0-9 -][^<>\~@;$^%{ }?]{1,50}?").Success);
        }
        static public bool isUsernameValid(string username)
        {
            return !(string.IsNullOrWhiteSpace(username) || string.IsNullOrEmpty(username) || !Regex.Match(username, @"^[A-Za-z0-9 -][^#&<>\~@;$^%{ }?]{1,20}?").Success);
        }
        static public bool isPasswordValid(string password)
        {
            return !(string.IsNullOrWhiteSpace(password) || string.IsNullOrEmpty(password) || !Regex.Match(password, @"^[A-Za-z0-9 -]{1,50}?").Success);    // Weak regex. Make a stronger one for real implementation
        }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> result = new List<ValidationResult>();

            if (Category < CategoryEnum.Patient || Category > CategoryEnum.ReferralPhysician)
            {
                result.Add(new ValidationResult("Invalid Category. Please select a valid option"));
            }
            if (!isNameValid(Name))
            {
                result.Add(new ValidationResult("User name must be 1-50 characters long, composed with letters, '-' and space"));
            }
            if (!isDOBValid(DOB))
            {
                result.Add(new ValidationResult("Date of birth's year must be between 1900 and 2100"));
            }
            if (!isEmailValid(Email))
            {
                result.Add(new ValidationResult("Email name must be 1-100 characters long, and follow the format: user@domain.com"));
            }
            if (!isPhoneValid(Phone))
            {
                result.Add(new ValidationResult("Phone must be 1-15 characters long, composed with numbers only"));
            }
            if (!isAddressValid(Address))
            {
                result.Add(new ValidationResult("Address must be 1-100 characters long, composed with letters, '-' and space"));
            }
            if (Gender < GenderEnum.Female || Gender > GenderEnum.NA)
            {
                result.Add(new ValidationResult("Invalid Gender. Please select a valid option"));
            }
            if (Category == CategoryEnum.Employee && !isEmplHireDateValid((DateTime)Empl_HireDate, DOB))
            {
                result.Add(new ValidationResult("Date of hire's year must be between 1950 and 2100. User must be at least 18 years old"));
            }
            if (Category == CategoryEnum.Employee && Empl_Salary < 1 || Empl_Salary >= 1000000)
            {
                result.Add(new ValidationResult("Salary must be a valid number, bigger than 0"));
            }
            //if (Category == CategoryEnum.ReferralPhysician && !isPhysClinicValid(Phys_Clinic))
            //{
            //    result.Add(new ValidationResult("Clinic name must be 1-50 characters long, composed with letters, '-' and space"));
            //}
            if (Category == CategoryEnum.Employee && !isUsernameValid(Username))
            {
                result.Add(new ValidationResult("Username must be 1-20 characters long, composed with letters, numbers, and these characters '-_.' only"));
            }
            if (Category == CategoryEnum.Employee && !isPasswordValid(Password))
            {
                result.Add(new ValidationResult("Password must be 1-50 characters long"));
            }

            return result;
        }
    }
}
