﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for CaseListDialog.xaml
    /// </summary>
    public partial class CaseListDialog : Window
    {
        medlabsystemEntities ctx;
        public CaseListDialog()
        {
            InitializeComponent();
            refreshCasesList();
        }

        void refreshCasesList()
        {
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    var CasesCol = (from ca in ctx.Cases.Include("User").Include("User1").Include("User2").Include("CasesTests") select ca).ToList<Case>();
                    lvCaseListDlgCasesList.ItemsSource = CasesCol;
                    lvCaseListDlgCasesList.Items.Refresh();
                    lvCaseListDlgCasesList.SelectedItem = null;
                }
                catch (SystemException) // ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
        }

        private void lvCaseListDlgCasesList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Case currCase = (Case)lvCaseListDlgCasesList.SelectedItem;
            CaseDialog.currCase = currCase; // assign selected case to currCase
            this.DialogResult = true; // close actual window
        }

        private void btCaseListDlgSearchByPatient_Click(object sender, RoutedEventArgs e)
        {
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    string searchStr = tbCaseListDlgSearch.Text;
                    var CasesColByPat = (from ca in ctx.Cases.Include("User").Include("User1").Include("User2") where (ca.User.Name.Contains(searchStr)) select ca).ToList<Case>(); // User references Patient
                    lvCaseListDlgCasesList.ItemsSource = CasesColByPat;
                    lvCaseListDlgCasesList.Items.Refresh();
                    lvCaseListDlgCasesList.SelectedItem = null;
                }
                catch (SystemException) // ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
        }

        private void btCaseListDlgSearchByPhysician_Click(object sender, RoutedEventArgs e)
        {
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    string searchStr = tbCaseListDlgSearch.Text;
                    var CasesColByPhys = (from ca in ctx.Cases.Include("User").Include("User1").Include("User2") where (ca.User1.Name.Contains(searchStr)) select ca).ToList<Case>(); // User1 references Physician
                    lvCaseListDlgCasesList.ItemsSource = CasesColByPhys;
                    lvCaseListDlgCasesList.Items.Refresh();
                    lvCaseListDlgCasesList.SelectedItem = null;
                }
                catch (SystemException) // ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
        }

        private void btCaseListDlgErase_Click(object sender, RoutedEventArgs e)
        {
            tbCaseListDlgSearch.Text = "";
            refreshCasesList();
        }

        private void btCaseListDlgClose_Click(object sender, RoutedEventArgs e)
        {
            CaseDialog.currCase = null;
            this.DialogResult = true; // close actual window
        }
    }
}
