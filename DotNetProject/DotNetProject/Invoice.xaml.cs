﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for Invoice.xaml
    /// </summary>
    public partial class Invoice : Window
    {
        medlabsystemEntities ctx;
        Case caseToPrint;
        List<LabTest> TestListToPrint = new List<LabTest>();
        public Invoice(Case caseToPrint)
        {
            InitializeComponent();
            this.caseToPrint = caseToPrint;
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    Case selectedCase = (from ca in ctx.Cases.Include("User").Include("User1").Include("User2").Include("CasesTests") where ca.Id == caseToPrint.Id select ca).FirstOrDefault<Case>();
                    foreach (CasesTest ct in selectedCase.CasesTests)
                    {
                        TestListToPrint.Add(ct.LabTest);
                    }
                }              
                 catch (SystemException) //ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
            // load case information
            lblInvoiceDlgCaseId.Content = caseToPrint.Id;
            lblInvoiceDlgDate.Content = caseToPrint.Transaction_Date;
            lblInvoiceDlgPatName.Content = caseToPrint.User.Name; // User = Patient
            lblInvoiceDlgPatId.Content = caseToPrint.User.Id;
            string[] address = caseToPrint.User.Address.Split(';');
            if (address.Length == 4)
            {
                lblInvoiceDlgPatAddress.Content = String.Format("{0}, {1} ({2}), {3}", address[0], address[1], address[2], address[3]);
            }
            lblInvoiceDlgPatPhone.Content = caseToPrint.User.Phone;
            lblInvoiceDlgPatEmail.Content = caseToPrint.User.Email;
            lblInvoiceDlgPhys.Content = String.Format("Dr. {0}", caseToPrint.User1.Name);  // User1 = Physician
            // load list of lab tests
            lvInvoiceDlgLabTestList.ItemsSource = TestListToPrint;
            lvInvoiceDlgLabTestList.Items.Refresh();
            lvInvoiceDlgLabTestList.SelectedItem = null;
            // load amounts
            lblInvoiceDlgTotalCost.Content = CaseDialog.totalCost.ToString("F");
            lblInvoiceDlgTax.Content = CaseDialog.tax.ToString("F");
            lblInvoiceDlgNetAmount.Content = CaseDialog.netAmount.ToString("F");
            lblInvoiceDlgTotalPaid.Content = CaseDialog.totalPaid.ToString("F");
            lblInvoiceDlgBalance.Content = CaseDialog.balance.ToString("F");
        }

        private void btInvoiceDlgPrint_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btDlgCaseCancel.Visibility = Visibility.Hidden;
                btInvoiceDlgPrint.Visibility = Visibility.Hidden;
                this.IsEnabled = false;
                PrintDialog printDialog = new PrintDialog();
                if (printDialog.ShowDialog() == true)
                {
                    printDialog.PrintVisual(print, "Invoice");
                }
            }
            catch (Exception) // ex
            {
                MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Printing error", MessageBoxButton.OK, MessageBoxImage.Error);
                // TODO: add ex.Message to logger file
            }
            finally
            {
                this.IsEnabled = true;
                btDlgCaseCancel.Visibility = Visibility.Visible;
                btInvoiceDlgPrint.Visibility = Visibility.Visible;
            }
        }
    }
}
