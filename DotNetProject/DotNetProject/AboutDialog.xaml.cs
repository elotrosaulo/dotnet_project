﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for AboutDialog.xaml
    /// </summary>
    public partial class AboutDialog : Window
    {
        public AboutDialog()
        {
            InitializeComponent();
            string about = "This software made as part of the requirements for the .NET and C#.NET Windows Development class of the Internet Programming and Development program (IPD-20) by:";
            tblockAbout.Text = about;
            string copyrightText = Convert.ToChar(174) + " 2020 All Rights Reserved.";
            lblCopyright.Content = copyrightText;
        }

        private void BtGoBack_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
