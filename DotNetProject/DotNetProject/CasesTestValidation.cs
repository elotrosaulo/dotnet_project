﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetProject
{
    public partial class CasesTest : IValidatableObject
    {
        static medlabsystemEntities ctx;
        public static bool isCaseValid(int caseId)
        {
            using (ctx = new medlabsystemEntities())
            {
                Case caseToValidate = (from ca in ctx.Cases where ca.Id == caseId select ca).FirstOrDefault<Case>();
                if (caseToValidate == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public static bool isLabTestValid(int testId)
        {
            using (ctx = new medlabsystemEntities())
            {
                LabTest testToValidate = (from lt in ctx.LabTests where lt.Id == testId select lt).FirstOrDefault<LabTest>();
                if (testToValidate == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public static bool isResultValid(double? testResult)
        {
            return !(testResult < 0 || testResult > 100000);
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> result = new List<ValidationResult>();
            if (!isCaseValid(Case_Id))
            {
                result.Add(new ValidationResult("Case not valid"));
            }
            if (!isLabTestValid(Test_Id))
            {
                result.Add(new ValidationResult("Lab Test not valid"));
            }
            if (!isResultValid(Result))
            {
                result.Add(new ValidationResult("Result not valid"));
            }
            return result;
        }

        public string ResultStr { get{ if (Result == null) return ""; else return Result + " " + LabTest.Unit; }}
    }
}
