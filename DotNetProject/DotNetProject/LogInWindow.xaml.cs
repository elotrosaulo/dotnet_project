﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for LogInWindow.xaml
    /// </summary>
    public partial class LogInWindow : Window
    {
        medlabsystemEntities ctx;

        public LogInWindow()
        {
            InitializeComponent();
            ctx = new medlabsystemEntities();
            //Globals.IsAdmin = false;
        }

        private void BtLogIn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string username = tbUsername.Text;
                string passwordInput = tbPassword.Password;
                string password = Globals.Encrypt(passwordInput);

                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(passwordInput))
                {
                    MessageBox.Show("Username and Password can't be empty. Please insert a valid user name and password", "Invalid user or password", MessageBoxButton.OK, MessageBoxImage.Hand);
                    return;
                }

                var userFromUsers = (from u in ctx.Users where u.Username.Equals(username) select u).FirstOrDefault<User>();
                if (userFromUsers != null)
                {
                    Globals.currentEmployee = userFromUsers;
                    if (userFromUsers.Empl_Position.Equals(PositionEnum.Manager))
                    {
                        Globals.IsAdmin = true;
                    }
                    if (userFromUsers.Password.Equals(password))
                    {
                        Globals.PlaySuccessSFX();
                        MainWindow mainWindow = new MainWindow();
                        this.Close();
                        mainWindow.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Wrong password. Please try again", "Invalid Password", MessageBoxButton.OK, MessageBoxImage.Hand);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Wrong user name. Please try again", "Invalid username", MessageBoxButton.OK, MessageBoxImage.Hand);
                    return;
                }
            }
            catch (SystemException) //ex
            {
                MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                // TODO: add ex.Message to logger file
            }
        }
    }
}
