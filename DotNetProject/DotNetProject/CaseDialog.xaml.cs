﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DotNetProject
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class CaseDialog : Window
    {
        medlabsystemEntities ctx;
        public static Case currCase; 
        public static User currPatCase; //User1
        public static User currPhysCase; //User2
        public static User currEmployee; //User3
        public static int currEmplId;
        public static decimal totalCost, tax, netAmount, totalPaid, balance;
        List<LabTest> CurrentTestList = new List<LabTest>();

        public CaseDialog()
        {
            InitializeComponent();
            resetAllFields();
            updateLabTestList(); // Feed list of LabTests to be selected
            currEmployee = Globals.currentEmployee;
            currEmplId = currEmployee.Id;
        }

        private void updateLabTestList() // Feed list of LabTests to be selected
        {
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    var LabTestCol = (from lt in ctx.LabTests orderby lt.Name select lt).ToList<LabTest>();
                    lvCaseDlgTestListByName.ItemsSource = LabTestCol;
                    lvCaseDlgTestListByName.Items.Refresh();
                    lvCaseDlgTestListByName.SelectedItem = null;
                }
                catch (SystemException) // ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
        }

        private void updateCaseTestList() // Update list of LabTests selected to be added to Case
        {
            lvCaseDialogTestList.ItemsSource = CurrentTestList;
            lvCaseDialogTestList.Items.Refresh();
            lvCaseDialogTestList.SelectedItem = null;
        } 

        private void updateAmounts()
        {
            tax = totalCost * 0.15m;
            netAmount = totalCost + tax;
            balance = netAmount - totalPaid;
            lblCaseDialogTotalCost.Content = totalCost.ToString("F");
            lblCaseDialogTax.Content = tax.ToString("F");
            lblCaseDialogNetAmount.Content = netAmount.ToString("F");
            lblCaseDialogBalance.Content = balance.ToString("F");
        }

        private void btDlgCaseSave_Click(object sender, RoutedEventArgs e)
        {
            if (currCase == null)
            {
                // Collect all data
                if (currPatCase == null || !Case.isUserValid(currPatCase.Id))
                {
                    lblCaseDlgErrorPat.Visibility = Visibility.Visible;
                    return;
                }
                int patientId = currPatCase.Id;
                if (currPhysCase == null || !Case.isUserValid(currPhysCase.Id))
                {
                    lblCaseDlgErrorPhys.Visibility = Visibility.Visible;
                    return;
                }
                int physicianId = currPhysCase.Id;
                DateTime transDate = DateTime.Today;
                if (dpCaseDialogDueDate.SelectedDate == null || !Case.isDueDateValid(dpCaseDialogDueDate.SelectedDate.Value))
                {
                    lblCaseDlgErrorDate.Visibility = Visibility.Visible;
                    return;
                }
                else
                {
                    lblCaseDlgErrorDate.Visibility = Visibility.Hidden;
                }
                DateTime dueDate = dpCaseDialogDueDate.SelectedDate.Value;
                // totalPaid is already retrieved on btCaseDlgSaveTotalPaid_Click
                if (CurrentTestList.Count() == 0)
                {
                    lblCaseDlgErrorTestList.Visibility = Visibility.Visible;
                    return;
                }              
                // add new case
                using (ctx = new medlabsystemEntities())
                {
                    try
                    {                      
                        // Add case to Cases table
                        Case caseToAdd = new Case { Patient_Id = patientId, Physician_Id = physicianId, Employee_Id = currEmplId, Transaction_Date = transDate, Due_Date = dueDate, Total_Paid = totalPaid };
                        ctx.Cases.Add(caseToAdd);
                        ctx.SaveChanges();
                        int caseId = caseToAdd.Id; // Entity framework by default follows each INSERT with SELECT SCOPE_IDENTITY() when auto-generated Ids are used.

                        // Add labTests to CasesTests table
                        foreach (LabTest lt in CurrentTestList)
                        {
                            CasesTest caseTest = new CasesTest { Case_Id = caseId, Test_Id = lt.Id, Status = CasesTestStatusEnum.Pending, Result = null };
                            ctx.CasesTests.Add(caseTest);
                        }
                        ctx.SaveChanges();
                        Globals.PlaySuccessSFX();
                        MessageBox.Show(this, "Case registered with success.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                        resetAllFields();
                    }
                    catch (DbEntityValidationException ex) // This should not happen! Comment message errors (or return statments) to be able to test it.
                    {
                        string errorMessage = "";
                        foreach (var error in ex.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                            {
                                errorMessage += validationError.ErrorMessage + "\n";
                            }
                        }
                        MessageBox.Show(this, "Error adding case: " + errorMessage, "Validation Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    catch (SystemException) // ex
                    {
                        MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                        // TODO: add ex.Message to logger file
                    }
                }
            }
            else // currCase != null
            {
                // update case (only due date and amount paid can be updated)
                // totalPaid is retrieved on btCaseDlgSaveTotalPaid_Click
                if (dpCaseDialogDueDate.SelectedDate == null)
                {
                    lblCaseDlgErrorDate.Visibility = Visibility.Visible;
                    return;
                }
                DateTime newDueDate = dpCaseDialogDueDate.SelectedDate.Value;           
                using (ctx = new medlabsystemEntities())
                {
                    try
                    {
                        // Update case on Cases table
                        Case caseToUpdate = (from ca in ctx.Cases where ca.Id == currCase.Id select ca).FirstOrDefault<Case>();
                        if (caseToUpdate != null)
                        {
                            caseToUpdate.Due_Date = newDueDate;
                            caseToUpdate.Total_Paid = totalPaid;
                            ctx.SaveChanges();
                            Globals.PlaySuccessSFX();
                            MessageBox.Show(this, "Case updated with success.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                            resetAllFields();
                        }
                        else
                        {
                            MessageBox.Show("Record to update not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information); // Fix me??
                        }                      
                    }
                    catch (DbEntityValidationException ex) // This should never happen! Comment message errors (or return statments) to be able to test it.
                    {
                        string errorMessage = "";
                        foreach (var error in ex.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                            {
                                errorMessage += validationError.ErrorMessage + "\n";
                            }
                        }
                        MessageBox.Show(this, "Error updating case: " + errorMessage, "Validation Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return; // keep dialog open
                    }
                    catch (SystemException) // ex
                    {
                        MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                        // TODO: add ex.Message to logger file
                    }
                }
            }         
        }

        private void btDlgCaseSearch_Click(object sender, RoutedEventArgs e)
        {
            resetAllFields();
            CaseListDialog caseListDialog = new CaseListDialog();
            if (caseListDialog.ShowDialog() == true)
            {
                if (currCase != null)
                {
                    imageCaseDlgSaveBt.Source = new BitmapImage(new Uri(@"/images/save.png", UriKind.Relative));
                    // feed all information on from currCase in Case window
                    currPatCase = currCase.User;
                    currPhysCase = currCase.User1;
                    int currCaseId = currCase.Id;                   
                    lblCaseDlgCaseId.Content = currCase.Id;
                    lblCaseDlgPatientId.Content = currPatCase.Id;
                    dpCaseDialogDueDate.SelectedDate = currCase.Due_Date;
                    lblCaseDlgCasePatName.Content = currPatCase.Name;
                    lblCaseDlgCasePatEmail.Content = currPatCase.Email;
                    string[] address = currPatCase.Address.Split(';');
                    if (address.Length != 4)
                    {
                        //MessageBox.Show("Incorrect address format. Address should have 4 fields only", "Error reading address", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    lblCaseDlgCasePatAddress.Content = String.Format("{0}, {1} ({2}), {3}", address[0], address[1], address[2], address[3]);
                    lblCaseDlgCasePhysName.Content = currPhysCase.Name;
                    // Recover CasesTests from currCase and pass to CurrentTestList              
                    CurrentTestList.Clear();
                    using (ctx = new medlabsystemEntities())
                    {
                        try
                        {
                            Case selectedCase = (from ca in ctx.Cases.Include("CasesTests") where ca.Id == currCaseId select ca).FirstOrDefault<Case>();
                            foreach (CasesTest ct in selectedCase.CasesTests)
                            {
                                CurrentTestList.Add(ct.LabTest);
                            }
                        }
                        catch (SystemException) // ex
                        {
                            MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                            // TODO: add ex.Message to logger file
                        }

                    }
                    updateCaseTestList(); // display CurrentTestList
                    // Calculate amounts to be shown 
                    foreach (LabTest lt in CurrentTestList)
                    {
                        totalCost += lt.Price;
                    }
                    if (currCase.Total_Paid != null)
                    {
                        totalPaid = (decimal)currCase.Total_Paid;
                        tbCaseDialogTotalPaid.Text = totalPaid +"";
                    }
                   else
                    {
                        totalPaid = 0;
                    }
                    updateAmounts();
                    // enable buttons
                    btDlgCaseDelete.IsEnabled = true;
                    btDlgCasePrint.IsEnabled = true;
                    // disable buttons (Case can be retrieved, but only amount paid and due date can be updated) 
                    btCaseDlgSearchPhysician.IsEnabled = false;
                    btDlgCaseSearchPatient.IsEnabled = false;
                    btCaseDlgAddTest.IsEnabled = false;
                    btCaseDlgRemoveTest.IsEnabled = false;
                }
            }
        }

        private void btDlgCaseDelete_Click(object sender, RoutedEventArgs e)
        {
            if (currCase != null)
            {
                MessageBoxResult result = MessageBox.Show(this, "Do you want to delete selected case?", "Delete Case", MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        int currId = currCase.Id;
                        using (ctx = new medlabsystemEntities())
                        {
                            try
                            {
                                Case caseToDelete = (from ca in ctx.Cases where ca.Id == currId select ca).FirstOrDefault<Case>();
                                if (caseToDelete != null)
                                {
                                    ctx.Cases.Remove(caseToDelete);
                                    ctx.SaveChanges();
                                    Globals.PlaySuccessSFX();
                                    MessageBox.Show(this, "Case deleted with success.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                    resetAllFields();
                                }
                                else
                                {
                                    MessageBox.Show("Record to delete not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                                }
                            }
                            catch (SystemException) // ex
                            {
                                MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                                // TODO: add ex.Message to logger file
                            }
                        }    
                        break;
                    case MessageBoxResult.No:
                        break;
                    default: // should not happen
                        MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Internal error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                }
            }
        }

        private void btDlgCaseClear_Click(object sender, RoutedEventArgs e)
        {
            resetAllFields();
        }

        private void btDlgCasePrint_Click(object sender, RoutedEventArgs e)
        {
            if (currCase != null)
            {
                Invoice invoice = new Invoice(currCase);
                if (invoice.ShowDialog() == true)
                {
                    return;
                }
            }      
        }

        private void btCaseDlgAddTest_Click(object sender, RoutedEventArgs e)
        {
            LabTest testToAdd = (LabTest)lvCaseDlgTestListByName.SelectedItem;
            if (testToAdd != null)
            {
                lblCaseDlgErrorTestList.Visibility = Visibility.Hidden;
                if (!CurrentTestList.Any(lt => lt.Id == testToAdd.Id))
                {
                    CurrentTestList.Add(testToAdd);
                    totalCost += testToAdd.Price;
                }               
            }
            updateCaseTestList();
            updateAmounts();
        }

        private void btCaseDlgRemoveTest_Click(object sender, RoutedEventArgs e)
        {
            LabTest testToRemove = (LabTest)lvCaseDialogTestList.SelectedItem;
            if (testToRemove != null)
            {
                CurrentTestList.Remove(testToRemove);
                totalCost -= testToRemove.Price;
            }
            updateCaseTestList();
            updateAmounts();
        }

        private void btDlgCaseSearchPatient_Click(object sender, RoutedEventArgs e)
        {
            PatientListDialog patListDialog = new PatientListDialog();
            if (patListDialog.ShowDialog() == true)
            {
                if (currPatCase != null)
                {
                    lblCaseDlgPatientId.Content = currPatCase.Id;
                    lblCaseDlgCasePatName.Content = currPatCase.Name;
                    lblCaseDlgCasePatEmail.Content = currPatCase.Email;
                    lblCaseDlgCasePatAddress.Content = currPatCase.Address;
                    lblCaseDlgErrorPat.Visibility = Visibility.Hidden;
                }
                else
                {
                    lblCaseDlgErrorPat.Visibility = Visibility.Visible;
                }
            }
        }

        private void btCaseDlgSearchPhysician_Click(object sender, RoutedEventArgs e)
        {
            PhysicianListDialog physicianListDialog = new PhysicianListDialog();
            if (physicianListDialog.ShowDialog() == true)
            {
                if (currPhysCase != null)
                {
                    lblCaseDlgCasePhysName.Content = currPhysCase.Name;
                    lblCaseDlgErrorPhys.Visibility = Visibility.Hidden;
                }
                else
                {
                    lblCaseDlgErrorPhys.Visibility = Visibility.Visible;
                }
            }
        }

        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tcTestName.IsSelected)
            {
                tbCaseDialogSearchTestByName.Text = "";
                updateLabTestList();
            }
            else if (tcCategory.IsSelected)
            {
                rbCategory_Checked(sender, e);
            }
            else
            {
                tbCaseDialogSearchTestByName.Text = "";
                rbHematology.IsChecked = true;
                updateLabTestList();
            }
        }

        private void tbCaseDialogTotalPaid_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!decimal.TryParse(tbCaseDialogTotalPaid.Text, out totalPaid))
            {
                if (tbCaseDialogTotalPaid.Text == "")
                {
                    lblCaseDlgErrorTotalPaid.Visibility = Visibility.Hidden;
                }
                else
                {
                    lblCaseDlgErrorTotalPaid.Visibility = Visibility.Visible;
                }
            }
            else if (!Case.isTotalPaidValid(totalPaid) || totalPaid > netAmount)
            {
                lblCaseDlgErrorTotalPaid.Visibility = Visibility.Visible;
            }
            else
            {
                lblCaseDlgErrorTotalPaid.Visibility = Visibility.Hidden;
            }
        }

        private void dpCaseDialogDueDate_LostFocus(object sender, RoutedEventArgs e)
        {
            if (dpCaseDialogDueDate.SelectedDate == null || !Case.isDueDateValid(dpCaseDialogDueDate.SelectedDate.Value))
            {
                lblCaseDlgErrorDate.Visibility = Visibility.Visible;
            }
            else
            {
                lblCaseDlgErrorDate.Visibility = Visibility.Hidden;
            }
        }

        private void tbCaseDialogSearchTestByName_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchTestStr = tbCaseDialogSearchTestByName.Text;
            using (ctx = new medlabsystemEntities())
            {
                try
                {
                    var LabTestColByName = (from lt in ctx.LabTests where lt.Name.Contains(searchTestStr) orderby lt.Name select lt).ToList<LabTest>();
                    lvCaseDlgTestListByName.ItemsSource = LabTestColByName;
                    lvCaseDlgTestListByName.Items.Refresh();
                    lvCaseDlgTestListByName.SelectedItem = null;
                }
                catch (SystemException) // ex
                {
                    MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                    // TODO: add ex.Message to logger file
                }
            }
        }

        private void rbCategory_Checked(object sender, RoutedEventArgs e)
        {
            if (tcCategory.IsSelected == true)
            {
                string searchByCatStr;
                if (rbBiochemistry.IsChecked == true) { searchByCatStr = "Biochemistry"; }
                else if (rbCoagulation.IsChecked == true) { searchByCatStr = "Coagulation"; }
                else if (rbHematology.IsChecked == true) { searchByCatStr = "Hematology"; }
                else if (rbImmunology.IsChecked == true) { searchByCatStr = "Immunology"; }
                else if (rbMicrobiology.IsChecked == true) { searchByCatStr = "Microbiology"; }
                else if (rbParasitology.IsChecked == true) { searchByCatStr = "Parasitology"; }
                else if (rbPathology.IsChecked == true) { searchByCatStr = "Pathology"; }
                else if (rbUrinalysis.IsChecked == true) { searchByCatStr = "Urinalysis"; }
                else if (rbVirology.IsChecked == true) { searchByCatStr = "Virology"; }
                else { return; }
                using (ctx = new medlabsystemEntities())
                {
                    try
                    {
                        var LabTestColByCat = (from lt in ctx.LabTests where lt.Category.ToString().Contains(searchByCatStr) orderby lt.Name select lt).ToList<LabTest>();
                        lvCaseDlgTestListByName.ItemsSource = LabTestColByCat;
                        lvCaseDlgTestListByName.Items.Refresh();
                        lvCaseDlgTestListByName.SelectedItem = null;
                    }
                    catch (SystemException) // ex
                    {
                        MessageBox.Show(this, "An unexpected error occurred. If this issue continues, please contact support.", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                        // TODO: add ex.Message to logger file
                    }
                }
            }          
        }

        private void btCaseDlgSaveTotalPaid_Click(object sender, RoutedEventArgs e)
        {
            if (!decimal.TryParse(tbCaseDialogTotalPaid.Text, out totalPaid))
            {
                if (tbCaseDialogTotalPaid.Text == "")
                {
                    totalPaid = 0;
                    lblCaseDlgErrorTotalPaid.Visibility = Visibility.Hidden;
                }
                else
                {
                    lblCaseDlgErrorTotalPaid.Visibility = Visibility.Visible;
                    tbCaseDialogTotalPaid.Text = "";
                    return;
                }
            }
            if (!Case.isTotalPaidValid(totalPaid) || totalPaid > netAmount)
            {
                lblCaseDlgErrorTotalPaid.Visibility = Visibility.Visible;
                tbCaseDialogTotalPaid.Text = "";
                return;
            }
            else
            {
                lblCaseDlgErrorTotalPaid.Visibility = Visibility.Hidden;
                updateAmounts();
            }   
        }
      
        private void resetAllFields()
        {
            currCase = null;
            currPatCase = null;
            currPhysCase = null;
            CurrentTestList.Clear();
            tbCaseDialogSearchTestByName.Text = "";
            updateCaseTestList();        
            dpCaseDialogDueDate.SelectedDate = DateTime.Today.AddDays(3);
            lblCaseDialogBalance.Content = "";
            lblCaseDialogNetAmount.Content = "";
            lblCaseDialogTax.Content = "";
            lblCaseDialogTotalCost.Content = "";
            lblCaseDlgCaseId.Content = "";
            lblCaseDlgCasePatAddress.Content = "";
            lblCaseDlgCasePatEmail.Content = "";
            lblCaseDlgCasePatName.Content = "Select a patient";
            lblCaseDlgCasePhysName.Content = "Select a referral physician";
            lblCaseDlgPatientId.Content = "";          
            tbCaseDialogSearchTestByName.Text = "";
            rbHematology.IsChecked = true;
            tbCaseDialogTotalPaid.Text = "";
            lvCaseDlgTestListByName.SelectedItem = null;
            totalCost = 0;
            totalPaid = 0;
            updateAmounts();
            btDlgCaseDelete.IsEnabled = false;
            btDlgCasePrint.IsEnabled = false;
            btCaseDlgAddTest.IsEnabled = true;
            btCaseDlgRemoveTest.IsEnabled = true;
            btCaseDlgSearchPhysician.IsEnabled = true;
            btDlgCaseSearchPatient.IsEnabled = true;
            lblCaseDlgErrorDate.Visibility = Visibility.Hidden;
            lblCaseDlgErrorPat.Visibility = Visibility.Hidden;
            lblCaseDlgErrorPhys.Visibility = Visibility.Hidden;
            lblCaseDlgErrorTotalPaid.Visibility = Visibility.Hidden;
            lblCaseDlgErrorTestList.Visibility = Visibility.Hidden;
            imageCaseDlgSaveBt.Source = new BitmapImage(new Uri(@"/images/plus.png", UriKind.Relative));
        }
    }
}