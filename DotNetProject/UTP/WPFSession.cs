﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTP
{
    public class WPFSession
    {
        // URL where the WinAppDriver service is available ​
        private const string WindowApplicationDriverUrl = "http://127.0.0.1:4723/";
        //Identifier of the application ( full path of the executable)
        // For Giovana's computer:
        private const string AppId = @"C:\Users\giova\Documents\DotNet_Project\DotNetProject\DotNetProject\bin\Debug\DotNetProject.exe";
        // For Saulo's computer:
        //private const string AppId = "C:\\Users\\6155278\\Documents\\Saulo Carranza\\DNProject\\dotnet_project\\DotNetProject\\DotNetProject\\bin\\Debug\\DotNetProject.exe";
        // Define a static property to store the session​
        protected static WindowsDriver<WindowsElement> session;

        // Create a new session
        public WindowsDriver<WindowsElement> createSession() {
                AppiumOptions opt = new AppiumOptions();
                opt.AddAdditionalCapability("app", AppId);
                opt.AddAdditionalCapability("deviceName", "WindowsPC");
                return session = new WindowsDriver<WindowsElement>(new Uri(WindowApplicationDriverUrl), opt);
        }

        public static void TearDown()
        {
            if (session != null)
            {
                session.Close();
                session.Quit();
                session = null;
            }
         }

    }
}
