﻿using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTP
{
    class WPFFunctions : WPFSession
    {
        WindowsDriver<WindowsElement> session;

        [Test]
        public void LogIn() {
            session = createSession();
            const string username = "admin";
            const string password = "admin";

            session.FindElementByAccessibilityId("tbUsername").SendKeys(username);
            session.FindElementByAccessibilityId("tbPassword").SendKeys(password);
            session.FindElementByAccessibilityId("btLogIn").Click();    // Opens a new window
        }

        [Test]
        public void CreateNewPatient()
        {
            const string name = "Pablo";
            const string dob = "04 / 04 / 1990";
            const string email = "pablo@gmail.com";
            const string phone = "514 963 5781";
            const string address = "56 Test Street";
            const string city = "Levis";
            const string postalCode = "G2J 1B2";

            LogIn();                                        
            session.SwitchTo().Window(session.WindowHandles.First());   // This line switches the session to the latest window
            session.FindElementByAccessibilityId("btUser").Click();
            session.SwitchTo().Window(session.WindowHandles.First());   // This line switches the session to the latest window
            session.FindElementByAccessibilityId("comboCategory").Click();
            session.FindElementByName("Patient").Click();
            session.FindElementByAccessibilityId("tbName").SendKeys(name);
            session.FindElementByAccessibilityId("dpBirthDate").SendKeys(dob);
            session.FindElementByAccessibilityId("comboGender").Click();
            session.FindElementByName("Male").Click();
            session.FindElementByAccessibilityId("tbEmail").SendKeys(email);
            session.FindElementByAccessibilityId("tbPhone").SendKeys(phone);
            session.FindElementByAccessibilityId("tbAddress").SendKeys(address);
            session.FindElementByAccessibilityId("tbCity").SendKeys(city);
            session.FindElementByAccessibilityId("tbPostalCode").SendKeys(postalCode);
            session.FindElementByAccessibilityId("comboRegion").Click();
            session.FindElementByName("QC").Click();
            // session.FindElementByAccessibilityId("btSave").Click();
            session.FindElementByAccessibilityId("Close").Click();
            session.SwitchTo().Window(session.WindowHandles.First()); // Close all windows
            TearDown(); // Close session
        }

        [Test]
        public void CreateNewEmployee()
        {
            const string name = "Ivan Labrecque";
            const string dob = "01 / 31 / 1980";
            const string email = "ivanl@gmail.com";
            const string phone = "514 852 7531";
            const string address = "2-789 Maissoneuve boul.";
            const string city = "Montreal";
            const string postalCode = "H1E 2B1";
            const string username = "ivan";
            const string password = "ivanpassword";
            const string salary = "82000";
            const string dateOfHire = "01 / 04 / 2020";

            LogIn();
            session.SwitchTo().Window(session.WindowHandles.First());   // This line switches the session to the latest window
            session.FindElementByAccessibilityId("btUser").Click();
            session.SwitchTo().Window(session.WindowHandles.First());   // This line switches the session to the latest window
            session.FindElementByAccessibilityId("comboCategory").Click();
            session.FindElementByName("Employee").Click();
            session.FindElementByAccessibilityId("tbName").SendKeys(name);
            session.FindElementByAccessibilityId("dpBirthDate").SendKeys(dob);
            session.FindElementByAccessibilityId("comboGender").Click();
            session.FindElementByName("Male").Click();
            session.FindElementByAccessibilityId("tbEmail").SendKeys(email);
            session.FindElementByAccessibilityId("tbPhone").SendKeys(phone);
            session.FindElementByAccessibilityId("tbAddress").SendKeys(address);
            session.FindElementByAccessibilityId("tbCity").SendKeys(city);
            session.FindElementByAccessibilityId("tbPostalCode").SendKeys(postalCode);
            session.FindElementByAccessibilityId("comboRegion").Click();
            session.FindElementByName("QC").Click();
            session.FindElementByAccessibilityId("tbUsername").SendKeys(username);
            session.FindElementByAccessibilityId("tbPassword").SendKeys(password);
            session.FindElementByAccessibilityId("comboRight3").Click();
            session.FindElementByName("Technologist").Click();
            session.FindElementByAccessibilityId("tbSalary").SendKeys(salary);
            session.FindElementByAccessibilityId("dpHireDate").SendKeys(dateOfHire);
            session.FindElementByAccessibilityId("btPhoto").Click();
            session.SwitchTo().Window(session.WindowHandles.First());   // This line switches the session to the latest window
            session.FindElementByName("Pictures").Click();
            session.FindElementByName("userplaceholder.png").Click();   // This only works if there is a "userplaceholder.png" in the Pictures folder (Please copy-paste from the Images folder on the project)
            session.FindElementByAccessibilityId("1").Click();
            //  session.FindElementByAccessibilityId("btSave").Click();
            session.FindElementByAccessibilityId("Close").Click();
            session.SwitchTo().Window(session.WindowHandles.First()); // Close all windows
            TearDown(); // Close session
        }

        [Test]
        public void CreateNewReferralPhysician()
        {
            const string name = "Sarah Oliver";
            const string dob = "01 / 31 / 1982";
            const string email = "sarah@gmail.com";
            const string phone = "514 963 0102";
            const string address = "451 Atwater Av.";
            const string city = "Montreal";
            const string postalCode = "H3E 1B2";
            const string clinic = "General Hospital";

            LogIn();
            session.SwitchTo().Window(session.WindowHandles.First());   // This line switches the session to the latest window
            session.FindElementByAccessibilityId("btUser").Click();
            session.SwitchTo().Window(session.WindowHandles.First());   // This line switches the session to the latest window
            session.FindElementByAccessibilityId("comboCategory").Click();
            session.FindElementByName("ReferralPhysician").Click();
            session.FindElementByAccessibilityId("tbName").SendKeys(name);
            session.FindElementByAccessibilityId("dpBirthDate").SendKeys(dob);
            session.FindElementByAccessibilityId("comboGender").Click();
            session.FindElementByName("Female").Click();
            session.FindElementByAccessibilityId("tbEmail").SendKeys(email);
            session.FindElementByAccessibilityId("tbPhone").SendKeys(phone);
            session.FindElementByAccessibilityId("tbAddress").SendKeys(address);
            session.FindElementByAccessibilityId("tbCity").SendKeys(city);
            session.FindElementByAccessibilityId("tbPostalCode").SendKeys(postalCode);
            session.FindElementByAccessibilityId("comboRegion").Click();
            session.FindElementByName("QC").Click();
            session.FindElementByAccessibilityId("tbClinic").SendKeys(clinic);
            session.FindElementByAccessibilityId("comboRight3").Click();
            session.FindElementByName("Neurology").Click();
            session.FindElementByAccessibilityId("btPhoto").Click();
            session.SwitchTo().Window(session.WindowHandles.First());   // This line switches the session to the latest window
            session.FindElementByName("Pictures").Click();
            session.FindElementByName("userplaceholder.png").Click();   // This only works if there is a "userplaceholder.png" in the Pictures folder (Please copy-paste from the Images folder on the project)
            session.FindElementByAccessibilityId("1").Click();
            //  session.FindElementByAccessibilityId("btSave").Click();
            session.FindElementByAccessibilityId("Close").Click();
            session.SwitchTo().Window(session.WindowHandles.First()); // Close all windows
            TearDown(); // Close session
        }


        [Test]
        public void CreateNewLabTest()
        {
            string labTestName = "LabTest 1";
            string unit = "mg/dL";
            string min = "5";
            string max = "10";
            string price = "10.50";
            
            LogIn();
            session.SwitchTo().Window(session.WindowHandles.First()); // This line switches the session to the Main window
            session.FindElementByAccessibilityId("btTest").Click();
            session.SwitchTo().Window(session.WindowHandles.First()); // This line switches the session to the LabTest window
            Assert.IsTrue(session.FindElementByAccessibilityId("btTestDlgSave").Enabled); // Test if Save button is enabled
            Assert.IsTrue(session.FindElementByAccessibilityId("btTestDlgSearch").Enabled);
            Assert.IsFalse(session.FindElementByAccessibilityId("btTestDlgDelete").Enabled);// Test if Delete button is not enabled
            Assert.IsTrue(session.FindElementByAccessibilityId("btTestDlgErase").Enabled);
            Assert.IsTrue(session.FindElementByAccessibilityId("btTestDlgCancel").Enabled);
            session.FindElementByAccessibilityId("tbTestDlgName").SendKeys(labTestName); // Fill up text box with content
            session.FindElementByAccessibilityId("comboTestDlgCategory").Click(); // Click on the combo list
            session.FindElementByName("Hematology").Click();// Choose radio button
            session.FindElementByAccessibilityId("tbTestDlgUnit").SendKeys(unit);
            session.FindElementByAccessibilityId("tbTestDlgMin").SendKeys(min);
            session.FindElementByAccessibilityId("tbTestDlgMax").SendKeys(max);
            session.FindElementByAccessibilityId("comboTestDlgSample").Click();
            session.FindElementByName("Urine").Click();
            session.FindElementByAccessibilityId("tbTestDlgPrice").SendKeys(price);
            // session.FindElementByAccessibilityId("btTestDlgSave").Click(); // Click on Save button
            session.FindElementByAccessibilityId("btTestDlgCancel").Click();
            session.SwitchTo().Window(session.WindowHandles.First()); // Close all windows
            TearDown(); // Close session
        }

        [Test]
        public void RecoverLabTest()
        {
            LogIn();
            session.SwitchTo().Window(session.WindowHandles.First());  // This line switches the session to the Main window
            session.FindElementByAccessibilityId("btTest").Click();
            session.SwitchTo().Window(session.WindowHandles.First());  // This line switches the session to the LabTest window
            session.FindElementByAccessibilityId("btTestDlgSearch").Click();
            session.SwitchTo().Window(session.WindowHandles.First());  // This line switches the session to the LabTestList window
            Actions actions = new Actions(session);
            actions
               .MoveToElement(session.FindElementByAccessibilityId("lvTestListDlgLabTests"), 40, 40)
               .DoubleClick()
               .Perform();
            session.SwitchTo().Window(session.WindowHandles.First()); // This line switches the session back to the LabTest window
            Assert.IsTrue(session.FindElementByAccessibilityId("btTestDlgSave").Enabled);
            Assert.IsTrue(session.FindElementByAccessibilityId("btTestDlgSearch").Enabled);
            Assert.IsTrue(session.FindElementByAccessibilityId("btTestDlgDelete").Enabled); // Delete button must be enable
            Assert.IsTrue(session.FindElementByAccessibilityId("btTestDlgErase").Enabled);
            Assert.IsTrue(session.FindElementByAccessibilityId("btTestDlgCancel").Enabled);
            session.FindElementByAccessibilityId("btTestDlgErase").Click();
            session.FindElementByAccessibilityId("btTestDlgCancel").Click();
            session.SwitchTo().Window(session.WindowHandles.First()); // Close all windows
            TearDown(); // Close session
        }

        [Test]
        public void CreateNewCase()
        {
            string amountPaid = "0.50";
            LogIn();
            session.SwitchTo().Window(session.WindowHandles.First());  // This line switches the session to the Main window
            session.FindElementByAccessibilityId("btCase").Click();
            session.SwitchTo().Window(session.WindowHandles.First());  // This line switches the session to the Case window
            Assert.IsTrue(session.FindElementByAccessibilityId("btDlgCaseSave").Enabled);
            Assert.IsTrue(session.FindElementByAccessibilityId("btDlgCaseSearch").Enabled);
            Assert.IsFalse(session.FindElementByAccessibilityId("btDlgCaseDelete").Enabled); // Delete button must be disable
            Assert.IsTrue(session.FindElementByAccessibilityId("btDlgCaseClear").Enabled);
            Assert.IsFalse(session.FindElementByAccessibilityId("btDlgCasePrint").Enabled); // Print button must be disable
            Assert.IsTrue(session.FindElementByAccessibilityId("btDlgCaseCancel").Enabled);
            var dueDateSelector = session.FindElementByAccessibilityId("dpCaseDialogDueDate");
            Assert.IsNotNull(dueDateSelector.Text);
            session.FindElementByAccessibilityId("btDlgCaseSearchPatient").Click();
            session.SwitchTo().Window(session.WindowHandles.First());  // This line switches the session to the Patient List window
            Actions actions1 = new Actions(session);
            actions1
               .MoveToElement(session.FindElementByAccessibilityId("lvPatListDlgPatientList"), 40, 40)
               .DoubleClick()
               .Perform();
            session.SwitchTo().Window(session.WindowHandles.First()); // This line switches the session back to the Case window
            session.FindElementByAccessibilityId("btCaseDlgSearchPhysician").Click();
            session.SwitchTo().Window(session.WindowHandles.First());  // This line switches the session to the Referral Physician List window
            // add two tests from list
            Actions actions2 = new Actions(session);
            actions2
               .MoveToElement(session.FindElementByAccessibilityId("lvPhysListDlgPhysicianList"), 40, 40)
               .DoubleClick()
               .Perform();
            session.SwitchTo().Window(session.WindowHandles.First()); // This line switches the session back to the Case window
            Actions actions3 = new Actions(session);
            actions3
               .MoveToElement(session.FindElementByAccessibilityId("lvCaseDlgTestListByName"), 40, 40)
               .Click()
               .Perform();
            session.FindElementByAccessibilityId("btCaseDlgAddTest").Click();
            Actions actions4 = new Actions(session);
            actions4
               .MoveToElement(session.FindElementByAccessibilityId("lvCaseDlgTestListByName"), 60, 60)
               .Click()
               .Perform();
            session.FindElementByAccessibilityId("btCaseDlgAddTest").Click();
            // switch to search lab test by category
            session.FindElementByAccessibilityId("tcCategory").Click();
            session.FindElementByAccessibilityId("rbCoagulation").Click();
            Actions actions5 = new Actions(session);
            actions5
               .MoveToElement(session.FindElementByAccessibilityId("lvCaseDlgTestListByName"), 40, 40)
               .Click()
               .Perform();
            session.FindElementByAccessibilityId("btCaseDlgAddTest").Click();
            // enter amount paid and compare balances
            string balance1 = session.FindElementByAccessibilityId("lblCaseDialogBalance").Text;
            session.FindElementByAccessibilityId("tbCaseDialogTotalPaid").SendKeys(amountPaid);
            session.FindElementByAccessibilityId("btCaseDlgSaveAmount").Click();
            string balance2 = session.FindElementByAccessibilityId("lblCaseDialogBalance").Text;
            Assert.AreNotEqual(balance1, balance2);
            //session.FindElementByAccessibilityId("btDlgCaseSave").Click();
            session.FindElementByAccessibilityId("btDlgCaseClear").Click();
            session.FindElementByAccessibilityId("btDlgCaseCancel").Click();
            session.SwitchTo().Window(session.WindowHandles.First());
            TearDown();
        }

        [Test]
        public void RecoverCase()
        {
            LogIn();
            session.SwitchTo().Window(session.WindowHandles.First());  // This line switches the session to the Main window
            session.FindElementByAccessibilityId("btCase").Click();
            session.SwitchTo().Window(session.WindowHandles.First());  // This line switches the session to the Case window
            session.FindElementByAccessibilityId("btDlgCaseSearch").Click();
            session.SwitchTo().Window(session.WindowHandles.First());  // This line switches the session to the Case List window
            Actions actions = new Actions(session);
            actions
               .MoveToElement(session.FindElementByAccessibilityId("lvCaseListDlgCasesList"), 40, 40)
               .DoubleClick()
               .Perform();
            session.SwitchTo().Window(session.WindowHandles.First()); // This line switches the session back to the Case window
            Assert.IsTrue(session.FindElementByAccessibilityId("btDlgCaseSave").Enabled);
            Assert.IsTrue(session.FindElementByAccessibilityId("btDlgCaseSearch").Enabled);
            Assert.IsTrue(session.FindElementByAccessibilityId("btDlgCaseDelete").Enabled);
            Assert.IsTrue(session.FindElementByAccessibilityId("btDlgCaseClear").Enabled);
            Assert.IsTrue(session.FindElementByAccessibilityId("btDlgCasePrint").Enabled);
            Assert.IsTrue(session.FindElementByAccessibilityId("btDlgCaseCancel").Enabled);
            Assert.IsFalse(session.FindElementByAccessibilityId("btDlgCaseSearchPatient").Enabled); // Search Patient button must be disable
            Assert.IsFalse(session.FindElementByAccessibilityId("btCaseDlgSearchPhysician").Enabled); // Search Referral Physician button must be disable
            Assert.IsFalse(session.FindElementByAccessibilityId("btCaseDlgAddTest").Enabled); // Add Test button must be disable
            Assert.IsFalse(session.FindElementByAccessibilityId("btCaseDlgRemoveTest").Enabled); // // Remove Test button must be disable
            session.FindElementByAccessibilityId("btDlgCaseClear").Click();
            session.FindElementByAccessibilityId("btDlgCaseCancel").Click();
            session.SwitchTo().Window(session.WindowHandles.First());
            TearDown();
        }

        
        [Test]
        public void RetrieveResultTest()
        {
            LogIn();
            session.SwitchTo().Window(session.WindowHandles.First());  // This line switches the session to the latest window
            session.FindElementByAccessibilityId("btResult").Click();
            session.SwitchTo().Window(session.WindowHandles.First());  // This line switches the session to the latest window
            session.FindElementByAccessibilityId("tbSearch").SendKeys("02");
            session.FindElementByAccessibilityId("btSearch").Click();
            session.SwitchTo().Window(session.WindowHandles.First());  // This line switches the session to the latest window
            session.FindElementByAccessibilityId("2").Click();
            session.FindElementByAccessibilityId("btClean").Click();
            session.FindElementByAccessibilityId("tbSearch").SendKeys("17");
            session.FindElementByAccessibilityId("btSearch").Click();
            session.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);           
            session.FindElementByAccessibilityId("btGoBack").Click();      
            //session.SwitchTo().Window(session.WindowHandles.First());  // Closing all windows pedestrian way...
            //session.FindElementByAccessibilityId("Close").Click();
            session.SwitchTo().Window(session.WindowHandles.First());
            TearDown();
        }
    }
}
            